'''
Take an integral file that has complex integrals, SDP file, and bas file and
turn it into a new SDP corresponding to the complex hamiltonian.  All blocks
will double in size.  Hopefully purified integrals will provide a better
non-broken solution to the SDP.

1) check to see if .sdp and .bas nad Integrals.dat are present.  
2) read Integrals.dat into program and construct reduced Hamiltonian.
3) read bas and reorder according to the following
    new ordering basis = {(orb, kval)} kval =
    {-int(K_L/2)+1,int(K_L/2)+1}%(2*np.pi)

    old ordering basis = {(orb, kval)} kval = { 0, K_L-1}%(2*np.pi)

4) generate new SDP and put in reordered K-2.
'''
from glob import glob
import os
import numpy as np
import sys
sys.path.append('/home/nick/pyncr/postprocess/')
import RDMUtil_03112013 as RDMUtil
from scipy.sparse import csr_matrix
from scipy.linalg import block_diag
from itertools import product
import re
#import SDPConvertC2R_noTRconstraints as SDPC2R
import SDPConvertC2R_TRconstraints as SDPC2R
#import SDPConvertC2R_TRconstraints_noAntisymmetry as SDPC2R
DEBUG = False

def SpinAdapt(gem_dim, r_dim):

    bas = dict(zip(range(gem_dim), product(range(1,r_dim+1),
        range(1,r_dim+1))))
    bas_rev = dict(zip( bas.values(), bas.keys()))

    #prepare for spin adapt
    D2ab_abas = {}
    D2ab_abas_rev = {}
    cnt = 0
    for xx in range(r_dim):
        for yy in range(xx+1,r_dim):
            D2ab_abas[cnt] = (xx+1, yy+1)
            D2ab_abas_rev[(xx+1, yy+1)] = cnt
            cnt += 1

    D2ab_sbas = {}
    D2ab_sbas_rev = {}
    cnt = 0
    for xx in range(r_dim):
        for yy in range(xx,r_dim):
            D2ab_sbas[cnt] = (xx+1, yy+1)
            D2ab_sbas_rev[(xx+1, yy+1)] = cnt
            cnt += 1

    
    U = np.zeros((gem_dim,gem_dim))
    cnt = 0
    for xx in D2ab_abas.keys():
        i,j = D2ab_abas[xx]
        x1 = bas_rev[(i,j)]
        x2 = bas_rev[(j,i)]
        U[x1, cnt ] = 1./np.sqrt(2)
        U[x2, cnt ] = -1./np.sqrt(2)
        cnt += 1

    for xx in D2ab_sbas.keys():

        i,j = D2ab_sbas[xx]
        x1 = bas_rev[(i,j)]
        x2 = bas_rev[(j,i)]
        
        if x1 == x2:
            U[x1, cnt ] = 1.0
        else:
            U[x1, cnt ] = 1./np.sqrt(2)
            U[x2, cnt ] = 1./np.sqrt(2)

        cnt += 1
    
    return U, D2ab_sbas, D2ab_sbas_rev, D2ab_abas, D2ab_abas_rev, bas, bas_rev


def BasOrder(basfile, D2ab_abas, D2ab_sbas):

  
    D2Abas, D2Sbas, D1bas, G2bas = RDMUtil.getBas(basfile)

    D2Abas_rev = map(lambda x: dict(zip(D2Abas[x].values(), D2Abas[x].keys())), range(len(D2Abas)) )
    D2Sbas_rev = map(lambda x: dict(zip(D2Sbas[x].values(), D2Sbas[x].keys())) , range(len(D2Sbas)) )
    D2ab_abas_rev = dict(zip(D2ab_abas.values(), D2ab_abas.keys()))
    D2ab_sbas_rev = dict(zip(D2ab_sbas.values(), D2ab_sbas.keys()))
 


    D2A_new_order = []
    for i in range(len(D2Abas)):
        for j in range(len(D2Abas[i])):
            D2A_new_order.append(D2ab_abas_rev[ D2Abas[i][j] ])
    D2S_new_order = []
    for i in range(len(D2Sbas)):
        for j in range(len(D2Sbas[i])):
            D2S_new_order.append(D2ab_sbas_rev[ D2Sbas[i][j] ])


    return D2A_new_order, D2S_new_order, D2Abas, D2Sbas, D1bas, G2bas

def BuildK2(r_dim, h1, VV, num_el):

    K2 = np.zeros(( (r_dim)**2, (r_dim)**2), dtype=complex)
    for i in range(r_dim):
        for j in range(r_dim):
            for k in range(r_dim):
                for l in range(r_dim):

                    if j == l:
                        K2[i*(r_dim)+j, k*(r_dim)+l] += (1./(num_el - 1))*(h1[i,k])
                    if i == k:
                        K2[i*(r_dim)+j, k*(r_dim)+l] += (1./(num_el -
                            1))*(h1[j,l])
                    
                    K2[i*(r_dim)+j, k*(r_dim)+l] += VV[i*(r_dim)+j, k*(r_dim)+l]

    return K2

def GetEnergy(D2c, K2, r_dim):


    U, D2ab_sbas, D2ab_sbas_rev, D2ab_abas, D2ab_abas_rev, D2ab_bas, D2ab_bas_rev = SpinAdapt(r_dim**2, r_dim)

    D2cSA = np.dot(U.T, np.dot(D2c, U))
    K2cSA = np.dot(U.T, np.dot(K2, U))
    
    K2cA = K2cSA[:len(D2ab_abas),:len(D2ab_abas)]
    K2cS = K2cSA[len(D2ab_abas):,len(D2ab_abas):]
    D2cA = D2cSA[:len(D2ab_abas),:len(D2ab_abas)]
    D2cS = D2cSA[len(D2ab_abas):,len(D2ab_abas):]

    ehf = np.trace(np.dot(3*K2cA, D2cA))
    ehf += np.trace(np.dot(K2cS, D2cS))

    return ehf




def Ints2tensors(Ints):

    #find basis rank
    cnt = 0
    while(Ints[cnt, 2] == 0):
        cnt += 1
    rdim = int(Ints[cnt - 1, 0])
    print "rdim ", rdim  
    indices = np.triu_indices(rdim)
    h1 = np.zeros((rdim,rdim),dtype=complex)
    h1[indices] = Ints[:cnt,4] + 1j*Ints[:cnt,5]
    h1 = h1 + h1.conjugate().T
    h1[np.diag_indices_from(h1)] /= 2


    h1_2 = np.zeros((rdim, rdim), dtype=complex)
    v2_row = []
    v2_col = []
    v2_dat = []
    cnt = 0
    while(int(Ints[cnt, 2 ]) == 0):
        i,j = int(Ints[cnt,0]), int(Ints[cnt,1])
        if i == j:
            h1_2[i -1 , j - 1] = Ints[cnt, 4] 
        else:
            h1_2[i -1 , j - 1] = Ints[cnt, 4] + 1j*Ints[cnt,5]
            h1_2[j -1 , i - 1] = Ints[cnt, 4] - 1j*Ints[cnt,5]
        cnt += 1


    while(True):

        try:
            i,j = int(Ints[cnt,0])-1, int(Ints[cnt,1])-1
            k,l = int(Ints[cnt,2])-1, int(Ints[cnt,3])-1
        except IndexError:
            break

        if i*rdim + j == k*rdim + l:
            v2_row.append(i*rdim + j)
            v2_col.append(k*rdim + l)
            v2_dat.append(Ints[cnt,4] ) # + 1j*Ints[cnt,5])
            #positive semidefinite matrices have real valued diagonals!!!!!
        else:
            v2_row.append(i*rdim + j)
            v2_col.append(k*rdim + l)
            v2_dat.append(Ints[cnt,4] + 1j*Ints[cnt,5])
            v2_row.append(k*rdim + l)
            v2_col.append(i*rdim + j)
            v2_dat.append(Ints[cnt,4] - 1j*Ints[cnt,5])

        cnt += 1
  
    VV = csr_matrix((v2_dat, (v2_row, v2_col)),shape=(rdim*rdim, rdim*rdim))
    
    return h1_2, VV, rdim

def checkHFENergy(nao, K2, rdim, K_L):

    RDM = np.zeros((nao,nao))
    for i in range(num_el/2):
        RDM[i,i] = 1.0
    print "Trace 1-RDM alpha block unit cell ", np.trace(RDM)
    DMO = block_diag(RDM,RDM)
    for i in range(2,K_L):
        DMO = block_diag(DMO,RDM)
    print "Trace 1-RDM alpha block k-space ", np.trace(DMO)
    D2 = np.kron(DMO,DMO)
    ehf = GetEnergy(D2, K2, rdim)
   
    ofile = min(glob("*.o"),key=lambda x: len(x))
    with open(ofile,'r') as fid:
        text = fid.read()
        p = re.search("(Total Energy)\s+[0-9.-]+",text)
        ref_energy = float(filter(None, p.group().split(' '))[-1])
        q = re.search("(Nuclear repulsion energy =)\s+[0-9.-]+",text)
        enuc = float(q.group().split("=")[-1])

    tmp1 = (ehf/K_L + enuc).real
    assert(np.abs(tmp1 - ref_energy) < float(1.0E-10))


def ReorderK2(rdim, K2):

    U, D2ab_sbas, D2ab_sbas_rev, D2ab_abas, D2ab_abas_rev, D2ab_bas, D2ab_bas_rev = SpinAdapt(rdim**2, rdim)
    D2A_new_order, D2S_new_order, D2Abas, D2Sbas, D1bas, G2bas = BasOrder(basfile, D2ab_abas, D2ab_sbas)


    RDM = np.zeros((nao,nao))
    for i in range(num_el/2):
        RDM[i,i] = 1.0
    DMO = block_diag(RDM,RDM)
    for i in range(2,K_L):
        DMO = block_diag(DMO,RDM)
    D2 = np.kron(DMO,DMO)


    K2SA = np.dot(U.T, np.dot(K2, U))
    D2SA = np.dot(U.T, np.dot(D2, U))
    assert(len(D2ab_abas ) == rdim*(rdim - 1)/2)
    K2ab_Anti = K2SA[0:rdim*(rdim - 1)/2, 0:rdim*(rdim-1)/2]
    K2ab_Sym = K2SA[rdim*(rdim - 1)/2:, rdim*(rdim-1)/2:]
    D2ab_Anti = D2SA[0:rdim*(rdim - 1)/2, 0:rdim*(rdim-1)/2]
    D2ab_Sym = D2SA[rdim*(rdim - 1)/2:, rdim*(rdim-1)/2:]
    #twoE = np.trace(np.dot(3*K2ab_Anti, D2ab_Anti))
    #twoE += np.trace(np.dot(K2ab_Sym, D2ab_Sym))
    #print "K2 HF energy = ", (twoE/K_L  + 1.38939393939689).real
    #print -0.875969338247654 
    print D2Abas[0]


def ReorderK2bas1_bas(rdim, K_L, nao, K2, basfile):

    #change the ordering the one-basis and the two basis are different.
    bas_base = {}
    bas_hub = {}
    kvec = range(-int(K_L/2) + 1, int(K_L/2) + 1)
    idx = 0
    for i in range(K_L):
        for j in range(nao):
            bas_base[idx] = (i, j)
            bas_hub[idx] = (kvec[i]%K_L, j)
            idx += 1
    bas_base_rev = dict(zip(bas_base.values(), bas_base.keys()))
    bas_hub_rev = dict(zip(bas_hub.values(), bas_hub.keys()))
   
    #build convert(i) = (hub_i) 
    #we put in bas_base index and get out bas_hub index
    convert = {}
    #for each key,val pair in bas_base we want to find the corresponding key,val pair in bas_hub
    for key_base, val_base in bas_base.iteritems(): 
        #print key_base, val_base, bas_hub_rev[val_base]
        convert[key_base] = bas_hub_rev[val_base]

    #print convert
    #now take d2ab_bas for my basis and build a new basis(label
    #set) for the reduced Hamiltonian
    U, D2ab_sbas, D2ab_sbas_rev, D2ab_abas, D2ab_abas_rev, D2ab_bas, D2ab_bas_rev = SpinAdapt(rdim**2, rdim)
    
    D2ab_bas_new = {}
    idx = 0
    canonical_order = []
    for key, val in D2ab_bas.iteritems():
        #my basis runs [0,N-1] David's runs [1,N]
        (i,j) = (convert[val[0]-1] , convert[val[1]-1] ) #convert to hubbard basis 
        D2ab_bas_new[key] = D2ab_bas_rev[(i + 1, j + 1)]
        canonical_order.append(D2ab_bas_rev[(i + 1, j + 1)])
        assert( D2ab_bas_rev[(i+1,j+1)] == i*rdim + j)

    
    #Now we need to spin adapt the new basis
    #   1) put into canonical ordering
    #   2) use U to spin adapt
    #   3) now map my new Hamiltonian to David's orderin found in basfile
    K2 = K2[:,canonical_order]
    K2 = K2[canonical_order,:]
    #np.save("K2ab", K2)
    RDM = np.zeros((nao,nao))
    for i in range(num_el/2):
        RDM[i,i] = 1.0
    DMO = block_diag(RDM,RDM)
    for i in range(2,K_L):
        DMO = block_diag(DMO,RDM)
    D2 = np.kron(DMO,DMO)
    D2 = D2[:,canonical_order]
    D2 = D2[canonical_order,:]

    #np.save("K2ab", K2)

    K2SA = np.dot(U.T, np.dot(K2, U))
    D2SA = np.dot(U.T, np.dot(D2, U))
    assert(len(D2ab_abas ) == rdim*(rdim - 1)/2)
    K2ab_Anti = K2SA[0:rdim*(rdim - 1)/2, 0:rdim*(rdim-1)/2]
    K2ab_Sym = K2SA[rdim*(rdim - 1)/2:, rdim*(rdim-1)/2:]
    D2ab_Anti = D2SA[0:rdim*(rdim - 1)/2, 0:rdim*(rdim-1)/2]
    D2ab_Sym = D2SA[rdim*(rdim - 1)/2:, rdim*(rdim-1)/2:]


    #twoE = np.trace(np.dot(3*K2ab_Anti, D2ab_Anti))
    #twoE += np.trace(np.dot(K2ab_Sym, D2ab_Sym))
    #print "K2 HF energy = ", (twoE/K_L  + 1.38939393939689).real
    #print -0.875969338247654 


    #########################################################################
    #
    #   K2 and D2 now ordered as D2ab_abas, and D2ab_sbas canonical order.
    #   Get David's ordering and reorder our basis to match.
    #
    #########################################################################

    #np.save("K2SA",K2SA)

    D2A_new_order, D2S_new_order, D2Abas, D2Sbas, D1bas, G2bas = BasOrder(basfile, D2ab_abas, D2ab_sbas)
    K2ab_Anti = K2ab_Anti[:,D2A_new_order]
    K2ab_Anti = K2ab_Anti[D2A_new_order,:]
    K2ab_Sym = K2ab_Sym[:,D2S_new_order]
    K2ab_Sym = K2ab_Sym[D2S_new_order,:]

    #np.save("K2SA_reorderd",block_diag(K2ab_Anti, K2ab_Sym))

    #########################################################################
    #
    #   Now break K2ab_Anti and K2ab_Sym into correct block sizes
    #
    #########################################################################

    D2Abas_blocksizes = map(lambda x: len(x), D2Abas)
    tK2ab_A = []
    for i in D2Abas_blocksizes:
        tK2ab_A.append( K2ab_Anti[:i,:i] )
        K2ab_Anti = K2ab_Anti[i:,i:]
    D2Sbas_blocksizes = map(lambda x: len(x), D2Sbas)
    tK2ab_S = []
    for i in D2Sbas_blocksizes:
        tK2ab_S.append(K2ab_Sym[:i,:i])
        K2ab_Sym = K2ab_Sym[i:,i:]

    
    return tK2ab_A, tK2ab_S

def CreateHFD2(nao, K_L, num_el):

    tmp = np.zeros((nao,nao),dtype=complex)
    for i in range(num_el/2):
        tmp[i,i] = 1.0

    if K_L == 1:
        D1K = tmp
    elif K_L == 2:
        D1K = block_diag(tmp,tmp)
    else:
        D1K = tmp
        for i in range(1,K_L):
            D1K = block_diag(D1K, tmp)


    D2HF = np.zeros(( nao*nao*K_L*K_L, nao*nao*K_L*K_L ), dtype=complex)
    rdim = nao*K_L
    for i in range(rdim):
        for j in range(rdim):
            for k in range(rdim):
                for l in range(rdim):
                    #ironically this is equivalent to the kronecker product!
                    D2HF[i*rdim + j, k*rdim + l] =  0.5*(D1K[i,k]*D1K[j,l] 
                            + D1K[j, l]*D1K[i,k] - D1K[j,k]*D1K[i,l] -
                            D1K[i,l]*D1K[j,k])

    print "Trace of D2HF ", np.trace(np.kron(D1K,D1K))

    return np.kron(D1K,D1K), D1K

if __name__=="__main__":

    nao = int(sys.argv[1])
    K_L = int(sys.argv[2])
    num_el = int(sys.argv[3])


    try:
        sdpfile = glob("*.sdp")[0]    
        basfile = glob("*.bas")[0]
        Intfile = "./ActSIntegrals.dat"
        #Intfile = "./Integrals.dat"
    except IndexError:
        print "ERROR: sdpfile pr basfile not found"
        sys.exit()

    print sdpfile, basfile, Intfile, os.getcwd()

    Ints = np.loadtxt(Intfile)
   
    H1, VV, rdim = Ints2tensors(Ints)

    K2 = BuildK2(rdim, H1, VV, num_el*K_L)

    if DEBUG == True:
        #Check HF ENERGY
        checkHFENergy(nao, K2, rdim, K_L)

    D2HF, D1K = CreateHFD2(nao, K_L, num_el)
    #K2ab_A, K2ab_S = ReorderK2bas1_bas(rdim, K_L, nao, K2, basfile)
    U, D2ab_sbas, D2ab_sbas_rev, D2ab_abas, D2ab_abas_rev, D2ab_bas, D2ab_bas_rev = SpinAdapt(rdim**2, rdim)
    K2SA = np.dot(U.T, np.dot(K2, U))
    D2HFSA = np.dot(U.T, np.dot(D2HF, U))
    K2ab_Anti = K2SA[0:rdim*(rdim - 1)/2, 0:rdim*(rdim-1)/2]
    K2ab_Sym = K2SA[rdim*(rdim - 1)/2:, rdim*(rdim-1)/2:]
    D2HF_Anti = D2HFSA[0:rdim*(rdim - 1)/2, 0:rdim*(rdim-1)/2]
    D2HF_Sym = D2HFSA[rdim*(rdim - 1)/2:, rdim*(rdim-1)/2:]
    print (np.trace(np.dot(3*K2ab_Anti, D2HF_Anti)) +
            np.trace(np.dot(K2ab_Sym, D2HF_Sym)))/K_L
    #-6.25059410130333
    
    D1K = 2*D1K 
    VV2 = VV.todense()
    Gmat = np.zeros_like(D1K)
    for i in range(rdim):
        for j in range(rdim):
            for k in range(rdim):
                for l in range(rdim):

                    Gmat[i,k] += D1K[l,j]*(VV2[i*rdim + j, k*rdim + l] - 0.5*VV[i*rdim + j, l*rdim + k])

    print np.trace(np.dot(D1K, H1 + 0.5*Gmat))/K_L
    print np.trace(np.dot(D1K, H1))/K_L
    print np.trace(np.dot(0.5*D1K, Gmat))/K_L
    print np.trace(np.dot(D1K, H1))/K_L + np.trace(np.dot(0.5*D1K, Gmat))/K_L


    D2A_new_order, D2S_new_order, D2Abas, D2Sbas, D1bas, G2bas = BasOrder(basfile, D2ab_abas, D2ab_sbas)
    K2ab_Anti = K2ab_Anti[:,D2A_new_order]
    K2ab_Anti = K2ab_Anti[D2A_new_order,:]
    K2ab_Sym = K2ab_Sym[:,D2S_new_order]
    K2ab_Sym = K2ab_Sym[D2S_new_order,:]

    D2Abas_blocksizes = map(lambda x: len(x), D2Abas)
    tK2ab_A = []
    for i in D2Abas_blocksizes:
        tK2ab_A.append( K2ab_Anti[:i,:i] )
        K2ab_Anti = K2ab_Anti[i:,i:]
    D2Sbas_blocksizes = map(lambda x: len(x), D2Sbas)
    tK2ab_S = []
    for i in D2Sbas_blocksizes:
        tK2ab_S.append(K2ab_Sym[:i,:i])
        K2ab_Sym = K2ab_Sym[i:,i:]

    SDPC2R.run1(nao=nao,K_L=K_L,sdpfile=sdpfile, K2Ainput=tK2ab_A, K2Sinput=tK2ab_S) 
    
