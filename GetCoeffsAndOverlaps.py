'''
get the coefficients and overlap matrices from files...crap format so we'll
have to be fancy with our scripting

'''

import os
import sys
from glob import glob
import numpy as np

def getCoeffs():

    #look in local directory for coeffs and report in sorted list
    CoeffFiles = sorted(glob("Coeffs*.dat"),key=lambda x:
            int(x.split('s')[1].split('.')[0]) )

    #print CoeffFiles
    Coeff_Mats = []
    for tfile in CoeffFiles:
        with open(tfile,'r') as fid:
            text =  fid.read()
            text = filter(None, text.split('\n') )
            dim = int(text[0])
            text = text[1:]
            tmp = np.zeros((dim,dim),dtype=complex)
            row_idx = 0
            for item in text:
                if 'col' in item:
                    col_idx = int( item.split('=')[1])
                    row_idx = 0
                else:
                    item.replace("(","")
                    item = item.translate(None, ''.join(['(',')']))
                    ritem, iitem = map(float, item.split(','))
                    tmp[row_idx, col_idx] = ritem + 1j*iitem
                    row_idx += 1
                    

        Coeff_Mats.append(tmp) 
    #for col in range(dim):
    #    print "col = ", col
    #    for row in range(dim):
    #        print  Coeff_Mats[2][row,col]

    return Coeff_Mats

def getOverlaps():

    with open("Overlaps.dat",'r') as fid:

        text = fid.read()
        text = filter(None, text.split('\n') )
        #partition into 2S+1 lists of matrices
        cell_idx = []
        sublist = []
        for ii in range(len(text)):
            if 'cell' in text[ii]:
                cell_idx.append(ii)

        dim = len(cell_idx)
        o_strings = []
        for i in range(dim):
            if i != dim -1 :
                o_strings.append(text[cell_idx[i]+1:cell_idx[i+1]])
            else:
                o_strings.append(text[cell_idx[i]+1:])
 
        #now map each of the o_strings into a matrix
        Omatrices = []
        for str in o_strings:
            str = map(lambda x: x.translate(None, ''.join(['[',']']) ), str )
            str = map(lambda x: filter(None, x.split(',')), str)
            str = map(lambda x: np.array(map(float, x)), str)
            tmp_mat = np.vstack(tuple(str))
            Omatrices.append(tmp_mat)

    return Omatrices

def getKinetic():

    with open("Kinetic.dat",'r') as fid:

        text = fid.read()
        text = filter(None, text.split('\n') )
        #partition into 2S+1 lists of matrices
        cell_idx = []
        sublist = []
        for ii in range(len(text)):
            if 'cell' in text[ii]:
                cell_idx.append(ii)

        dim = len(cell_idx)
        o_strings = []
        for i in range(dim):
            if i != dim -1 :
                o_strings.append(text[cell_idx[i]+1:cell_idx[i+1]])
            else:
                o_strings.append(text[cell_idx[i]+1:])
 
        #now map each of the o_strings into a matrix
        Omatrices = []
        for str in o_strings:
            str = map(lambda x: x.translate(None, ''.join(['[',']']) ), str )
            str = map(lambda x: filter(None, x.split(',')), str)
            str = map(lambda x: np.array(map(float, x)), str)
            tmp_mat = np.vstack(tuple(str))
            Omatrices.append(tmp_mat)

    return Omatrices


def getExchange():

    with open("Exchange.dat",'r') as fid:

        text = fid.read()
        text = filter(None, text.split('\n') )
        #partition into 2S+1 lists of matrices
        cell_idx = []
        sublist = []
        for ii in range(len(text)):
            if 'cell' in text[ii]:
                cell_idx.append(ii)

        dim = len(cell_idx)
        o_strings = []
        for i in range(dim):
            if i != dim -1 :
                o_strings.append(text[cell_idx[i]+1:cell_idx[i+1]])
            else:
                o_strings.append(text[cell_idx[i]+1:])
 
        #now map each of the o_strings into a matrix
        Omatrices = []
        for str in o_strings:
            str = map(lambda x: x.translate(None, ''.join(['[',']']) ), str )
            str = map(lambda x: filter(None, x.split(',')), str)
            str = map(lambda x: np.array(map(float, x)), str)
            tmp_mat = np.vstack(tuple(str))
            Omatrices.append(tmp_mat)

    return Omatrices



def getCoulomb():

    with open("Coulomb.dat",'r') as fid:

        text = fid.read()
        text = filter(None, text.split('\n') )
        #partition into 2S+1 lists of matrices
        cell_idx = []
        sublist = []
        for ii in range(len(text)):
            if 'cell' in text[ii]:
                cell_idx.append(ii)

        dim = len(cell_idx)
        o_strings = []
        for i in range(dim):
            if i != dim -1 :
                o_strings.append(text[cell_idx[i]+1:cell_idx[i+1]])
            else:
                o_strings.append(text[cell_idx[i]+1:])
 
        #now map each of the o_strings into a matrix
        Omatrices = []
        for str in o_strings:
            str = map(lambda x: x.translate(None, ''.join(['[',']']) ), str )
            str = map(lambda x: filter(None, x.split(',')), str)
            str = map(lambda x: np.array(map(float, x)), str)
            tmp_mat = np.vstack(tuple(str))
            Omatrices.append(tmp_mat)

    return Omatrices



def getPotential():

    with open("Potential.dat",'r') as fid:

        text = fid.read()
        text = filter(None, text.split('\n') )
        #partition into 2S+1 lists of matrices
        cell_idx = []
        sublist = []
        for ii in range(len(text)):
            if 'cell' in text[ii]:
                cell_idx.append(ii)

        dim = len(cell_idx)
        o_strings = []
        for i in range(dim):
            if i != dim -1 :
                o_strings.append(text[cell_idx[i]+1:cell_idx[i+1]])
            else:
                o_strings.append(text[cell_idx[i]+1:])
 
        #now map each of the o_strings into a matrix
        Omatrices = []
        for str in o_strings:
            str = map(lambda x: x.translate(None, ''.join(['[',']']) ), str )
            str = map(lambda x: filter(None, x.split(',')), str)
            str = map(lambda x: np.array(map(float, x)), str)
            tmp_mat = np.vstack(tuple(str))
            Omatrices.append(tmp_mat)

    return Omatrices




if __name__=="__main__":
    
    coeff_mats = getCoeffs()

    Omats = getOverlaps()
