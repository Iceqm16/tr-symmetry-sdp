'''
Make the the *CAVrun.pbs file for doing active space calculation

'''

import sys
import os
from glob import glob
import re

def writePBSmakeSDP(runname, RDMlevel,nodes,ppn, K_L, M, num_el):

    #print runname 
    runname += "C2RSDP"
    Text = '###PBS submission Script\n\n'
    INPUT = '#PBS -N ' + runname + '\n'
    INPUT = INPUT + '#PBS -e ' + runname + '.e' + '\n'
    INPUT = INPUT + '#PBS -o ' + runname + '.o' + '\n'
    INPUT = INPUT + '#PBS -l nodes=%i:ppn=%i\n\n' % (nodes,ppn)

    runinfo = 'module load epd\n'    
    run = 'python ' +\
    '/home/nick/pyncr/postprocess/PBC/pybin/complex2realSDP.py ' + '%i %i %i'%(
            M, K_L, num_el)
    

    setEnv = 'SCR_PATH=\"/state/partition1/scr/nick\"\n\n'
    setEnv = setEnv + 'if [ ! -e $SCR_PATH ]; then\nmkdir -p $SCR_PATH\nfi\n\n'
    setEnv = setEnv + 'cd $PBS_O_WORKDIR\n\n'

    
    Text = Text + INPUT + runinfo + setEnv + run

    fid = open(runname+'.pbs','w')
    fid.write(Text)
    fid.close() 



def writemakesdp(rdmlevel,orblist='NULL'):
    runname = glob('*.ei')[0].split('.')[0] 
    print 'generating sdp inputs for run named: %s' % runname 
    
    text = '#input ei file;\n'
    input = 'str := \"%s\";\n' % runname
    input = input + 'ifile := \"%s/\"||str||\".ei\";\n' % (os.getcwd())
    output = '# output sdp file;\n'
    output = output + 'ofile := \"%s/\"||str||\"%s.sdp\";\n' %(os.getcwd(),rdmlevel)
    method = '# method (\"d\",\"dq\",\"dqg\", or \"dqgt\");\n'
    method = method + 'method := \"%s\";\n' % rdmlevel
    orb = '# list of orbital indices of h atoms ([null] or [1,2]);\n'
    orb = orb + 'nH := [%s];\n' % orblist
    bas = '# optional basis set file;\n'
    bas = bas + 'bfile := \"%s/\"||str||\"%s.bas\";\n' % (os.getcwd(),rdmlevel)
    text = text + input + output + method + orb + bas 

    text = text + '# sdprdm;\nkernelopts(printbytes=false);\n'
    text = text + 'read \"/share/apps/rdmchem/makesdp/r67H/BIN/sdpRDMv67H.m\";\n'
    text = text + 'makeSDP(ifile,ofile,method,nH,bfile);\n'

    fid = open('makeSDP','w')
    fid.write(text)
    fid.close() 


if __name__=="__main__":

    pwd = os.getcwd()

    if len(sys.argv) != 5:
        print "first input is directory"
        print "second input is K_L"
        print "third input is M"
        print "fourth is num_el"
        sys.exit()

    K_L = int(sys.argv[2])
    M = int(sys.argv[3])
    num_el = int(sys.argv[4])
    os.chdir(sys.argv[1])

    dirs = filter(os.path.isdir, os.listdir('./'))

    for dd in dirs:

        os.chdir(dd)

        K_L = int(dd.split('K')[1].split('p')[0])

        writePBSmakeSDP(dd,'DQG',1,1,K_L,M, num_el)

        #open file and write eifile
        os.chdir("../")
        
