'''
Construct Core Hamiltonian, Active and Virtual Hamiltonian.  

The previous programs partitioned into Core and Valence.  This program will
partition into core, active, and valence sets.  In general we should be able to
pass this program a string of the orbitals we want in each set.  enforce that
they are appropriately summed.

Also this program produces a file with core energy printed.
'''

import numpy as np
import sys
from scipy.linalg import block_diag
from scipy.sparse import coo_matrix, csr_matrix
from itertools import product
from matplotlib import pyplot as plt
from glob import glob
import re
import os
os.chdir(sys.argv[1])
sys.path.append(os.getcwd())
from CAVprep_input import *
#import RDMUtil_03112013 as RDMUtil


K_L = 4


def sparse_block_diag(mats, format=None, dtype=None):

    from scipy.sparse import bmat, issparse
    """
    Build a block diagonal sparse matrix from provided matrices.
    .. versionadded:: 0.11.0
    Parameters
    ----------
    A, B, ... : sequence of matrices
        Input matrices.
    format : str, optional
        The sparse format of the result (e.g. "csr").  If not given, the matrix
        is returned in "coo" format.
    dtype : dtype specifier, optional
        The data-type of the output matrix.  If not given, the dtype is
        determined from that of `blocks`.
    Returns
    -------
    res : sparse matrix
    See Also
    --------
    bmat, diags
    Examples
    --------
    >>> A = coo_matrix([[1, 2], [3, 4]])
    >>> B = coo_matrix([[5], [6]])
    >>> C = coo_matrix([[7]])
    >>> block_diag((A, B, C)).todense()
    matrix([[1, 2, 0, 0],
            [3, 4, 0, 0],
            [0, 0, 5, 0],
            [0, 0, 6, 0],
            [0, 0, 0, 7]])
    """
    nmat = len(mats)
    rows = []
    for ia, a in enumerate(mats):
        row = [None]*nmat
        if issparse(a):
            row[ia] = a
        else:
            row[ia] = coo_matrix(a)
        rows.append(row)
    return bmat(rows, format=format, dtype=dtype)



def symmeterizeV2(VV, K_L, nao):


    """
    enforce 4 fold symmetry of complex two-electron integrals
    by taking average over equivalent integrals.
    VV should be in physics notation ordering
    """

    kvec = map(lambda x: 2*np.pi*x/K_L, range(K_L))
    kveci = range(K_L)

    basidx = {}
    for i in range(nao*K_L):
        basidx[i] = (kveci[i/nao], i%nao + 1)

    basrev = dict(zip(basidx.values(), basidx.keys()))
    
    d2bas = list(product(range(K_L*nao ), range(K_L*nao) ) )

    d2bas = dict(zip(range(len(d2bas)), d2bas))

    tVV = np.zeros_like(VV)

    for x in range(VV.shape[0]):
        for y in range(x, VV.shape[1]): #loop over upper triangle, we already enforced hermaticity

            k,l = d2bas[y]
            i,j = d2bas[x]

            #now print all symmetric things
            if (np.abs( np.linalg.norm(VV[i*K_L*nao + j, k*K_L*nao + l] )  ) >
                    float(1.0E-14) ):
                #print VV[i*K_L*nao + j, k*K_L*nao + l]
                #print VV[k*K_L*nao + l, i*K_L*nao + j].real - 1j*VV[k*K_L*nao + l, i*K_L*nao + j].imag
                #print VV[j*K_L*nao + i, l*K_L*nao + k]
                #print VV[l*K_L*nao + k, j*K_L*nao + i].real - 1j*VV[l*K_L*nao + k, j*K_L*nao + i].imag
     
                tmp =  (  VV[i*K_L*nao + j, k*K_L*nao + l] + 
                          (VV[k*K_L*nao + l, i*K_L*nao + j].real 
                          - 1j*VV[k*K_L*nao + l, i*K_L*nao + j].imag ) +
                            VV[j*K_L*nao + i, l*K_L*nao + k] + 
                          ( VV[l*K_L*nao + k, j*K_L*nao + i].real -
                          1j*VV[l*K_L*nao + k, j*K_L*nao + i].imag ) )
                tmp = tmp/4.
                #print ""

                tVV[i*K_L*nao + j, k*K_L*nao + l] = tmp
                tVV[k*K_L*nao + l, i*K_L*nao + j] = tmp.conjugate()
                tVV[j*K_L*nao + i, l*K_L*nao + k] = tmp 
                tVV[l*K_L*nao + k, j*K_L*nao + i] = tmp.conjugate()

                #print tVV[i*K_L*nao + j, k*K_L*nao + l]
                #print tVV[k*K_L*nao + l, i*K_L*nao + j].conjugate()
                #print tVV[j*K_L*nao + i, l*K_L*nao + k]
                #print tVV[l*K_L*nao + k, j*K_L*nao + i].conjugate()

                #print ""


    return tVV

def symmeterizeV2_2(VV, K_L, nao , d2bas):


    """
    enforce 4 fold symmetry of complex two-electron integrals
    by taking average over equivalent integrals.
    VV should be in physics notation ordering
    """

    print d2bas[0]
    sys.exit()
    #kvec = map(lambda x: 2*np.pi*x/K_L, range(K_L))
    #kveci = range(K_L)

    #basidx = {}
    #for i in range(nao*K_L):
    #    basidx[i] = (kveci[i/nao], i%nao + 1)

    #basrev = dict(zip(basidx.values(), basidx.keys()))
    #
    #d2bas = list(product(range(K_L*nao ), range(K_L*nao) ) )

    #d2bas = dict(zip(range(len(d2bas)), d2bas))

    tVV = np.zeros_like(VV)

    for mm in range(tVV.shape[0]):
        for x in range(VV[mm].shape[0]):
            for y in range(x, VV[mm].shape[1]): #loop over upper triangle, we already enforced hermaticity

                k,l = d2bas[mm][y]
                i,j = d2bas[mm][x]

                #now print all symmetric things
                if (np.abs( np.linalg.norm(VV[i*K_L*nao + j, k*K_L*nao + l] )  ) >
                        float(1.0E-14) ):
                    #print VV[i*K_L*nao + j, k*K_L*nao + l]
                    #print VV[k*K_L*nao + l, i*K_L*nao + j].real - 1j*VV[k*K_L*nao + l, i*K_L*nao + j].imag
                    #print VV[j*K_L*nao + i, l*K_L*nao + k]
                    #print VV[l*K_L*nao + k, j*K_L*nao + i].real - 1j*VV[l*K_L*nao + k, j*K_L*nao + i].imag
         
                    tmp =  (  VV[i*K_L*nao + j, k*K_L*nao + l] + 
                              (VV[k*K_L*nao + l, i*K_L*nao + j].real 
                              - 1j*VV[k*K_L*nao + l, i*K_L*nao + j].imag ) +
                                VV[j*K_L*nao + i, l*K_L*nao + k] + 
                              ( VV[l*K_L*nao + k, j*K_L*nao + i].real -
                              1j*VV[l*K_L*nao + k, j*K_L*nao + i].imag ) )
                    tmp = tmp/4.
                    #print ""

                    tVV[i*K_L*nao + j, k*K_L*nao + l] = tmp
                    tVV[k*K_L*nao + l, i*K_L*nao + j] = tmp.conjugate()
                    tVV[j*K_L*nao + i, l*K_L*nao + k] = tmp 
                    tVV[l*K_L*nao + k, j*K_L*nao + i] = tmp.conjugate()

                    #print tVV[i*K_L*nao + j, k*K_L*nao + l]
                    #print tVV[k*K_L*nao + l, i*K_L*nao + j].conjugate()
                    #print tVV[j*K_L*nao + i, l*K_L*nao + k]
                    #print tVV[l*K_L*nao + k, j*K_L*nao + i].conjugate()

                    #print ""


        return tVV



    
def blockV2(K_L, nao):


    kvec = map(lambda x: 2*np.pi*x/K_L, range(K_L))
    kveci = range(K_L)

    basidx = {}
    for i in range(nao*K_L):
        basidx[i] = (kveci[i/nao], i%nao )

    basrev = dict(zip(basidx.values(), basidx.keys()))

    D2bas = list(product(range(nao*K_L), range(nao*K_L)))
    new_order = []
    new_bas = []
    for i in kveci:
        cnt = 0
        tmp_bas = []
        for bb in range(len(D2bas)):
            if (basidx[D2bas[bb][0]][0] + basidx[D2bas[bb][1]][0])%K_L == i:
                new_order.append(bb)
                tmp_bas.append(D2bas[bb]) 
                cnt += 1
        new_bas.append(tmp_bas)

    assert(len(D2bas)==len(new_order))

    old_order = []
    for i in range(len(D2bas)):
        for n in range(len(new_order)):
            if i == new_order[n]:
                old_order.append(n)
   
    return new_order, old_order, new_bas


def BasOrder(basfile, D2ab_abas, D2ab_sbas):

  
    D2Abas, D2Sbas, D1bas, G2bas = RDMUtil.getBas(basfile)

    D2Abas_rev = map(lambda x: dict(zip(D2Abas[x].values(), D2Abas[x].keys())), range(len(D2Abas)) )
    D2Sbas_rev = map(lambda x: dict(zip(D2Sbas[x].values(), D2Sbas[x].keys())) , range(len(D2Sbas)) )
    D2ab_abas_rev = dict(zip(D2ab_abas.values(), D2ab_abas.keys()))
    D2ab_sbas_rev = dict(zip(D2ab_sbas.values(), D2ab_sbas.keys()))
  
    D2A_new_order = []
    for i in range(len(D2Abas)):
        for j in range(len(D2Abas[i])):
            D2A_new_order.append(D2ab_abas_rev[ D2Abas[i][j] ])
    D2S_new_order = []
    for i in range(len(D2Sbas)):
        for j in range(len(D2Sbas[i])):
            D2S_new_order.append(D2ab_sbas_rev[ D2Sbas[i][j] ])


    return D2A_new_order, D2S_new_order, D2Abas, D2Sbas, D1bas, G2bas

def hermaticity2(VV_2, K_L, nao, new_bas):

    """
    enforce hermaticity 

    """
    for mat in VV: 
        for x in range(mat.shape[0]):
            for y in range(x,mat.shape[1]):

                tmp_r = (mat[x,y].real + mat[y,x].real)/2.
                tmp_i = (mat[x,y].imag - mat[y,x].imag)/2.

                mat[x,y] = tmp_r + 1j*tmp_i
                mat[y,x] = tmp_r - 1j*tmp_i

    return VV
 

def hermaticity(VV, K_L, nao):

    """
    enforce hermaticity 

    """

    for x in range(VV.shape[0]):
        for y in range(x,VV.shape[1]):

            tmp_r = (VV[x,y].real + VV[y,x].real)/2.
            tmp_i = (VV[x,y].imag - VV[y,x].imag)/2.

            VV[x,y] = tmp_r + 1j*tmp_i
            VV[y,x] = tmp_r - 1j*tmp_i

    return VV
    
def SpinAdapt(gem_dim, r_dim):

    bas = dict(zip(range(gem_dim), product(range(1,r_dim+1),
        range(1,r_dim+1))))
    bas_rev = dict(zip( bas.values(), bas.keys()))

    #prepare for spin adapt
    D2ab_abas = {}
    D2ab_abas_rev = {}
    cnt = 0
    for xx in range(r_dim):
        for yy in range(xx+1,r_dim):
            D2ab_abas[cnt] = (xx+1, yy+1)
            D2ab_abas_rev[(xx+1, yy+1)] = cnt
            cnt += 1

    D2ab_sbas = {}
    D2ab_sbas_rev = {}
    cnt = 0
    for xx in range(r_dim):
        for yy in range(xx,r_dim):
            D2ab_sbas[cnt] = (xx+1, yy+1)
            D2ab_sbas_rev[(xx+1, yy+1)] = cnt
            cnt += 1

    
    U = np.zeros((gem_dim,gem_dim))
    cnt = 0
    for xx in D2ab_abas.keys():
        i,j = D2ab_abas[xx]
        x1 = bas_rev[(i,j)]
        x2 = bas_rev[(j,i)]
        U[x1, cnt ] = 1./np.sqrt(2)
        U[x2, cnt ] = -1./np.sqrt(2)
        cnt += 1

    for xx in D2ab_sbas.keys():

        i,j = D2ab_sbas[xx]
        x1 = bas_rev[(i,j)]
        x2 = bas_rev[(j,i)]
        
        if x1 == x2:
            U[x1, cnt ] = 1.0
        else:
            U[x1, cnt ] = 1./np.sqrt(2)
            U[x2, cnt ] = 1./np.sqrt(2)

        cnt += 1
    
    return U, D2ab_sbas, D2ab_sbas_rev, D2ab_abas, D2ab_abas_rev, bas, bas_rev


def BuildK2(r_dim, h1, VV, num_el):

    K2 = np.zeros(( (r_dim)**2, (r_dim)**2), dtype=complex)
    for i in range(r_dim):
        for j in range(r_dim):
            for k in range(r_dim):
                for l in range(r_dim):

                    if j == l:
                        K2[i*(r_dim)+j, k*(r_dim)+l] += (1./(num_el - 1))*(h1[i,k])
                    if i == k:
                        K2[i*(r_dim)+j, k*(r_dim)+l] += (1./(num_el - 1))*(h1[j,l])
                    
                    K2[i*(r_dim)+j, k*(r_dim)+l] += VV[i*(r_dim)+j, k*(r_dim)+l]

    return K2

def CoreEnergy(core_set, DMO, VV, h1, ):

    D_core = DMO[core_set,:]
    D_core = D_core[:,core_set]
    h1_core = h1[core_set,:]
    h1_core = h1_core[:,core_set]


    #Construct 2-electron Core set
    VV_core = np.zeros((len(core_set)**2, len(core_set)**2), dtype=complex)
    for i in range(len(core_set)):
        for j in range(len(core_set)):

            for k in range(len(core_set)):
                for l in range(len(core_set)):

                    a = core_set[i]
                    b = core_set[j]
                    p = core_set[k]
                    q = core_set[l]

                    VV_core[i*len(core_set)+j, k*len(core_set)+l] =  VV[a*nao*K_L + b, p*nao*K_L + q ]


    #construct G matrix 
    G = np.zeros_like(h1_core)
    for i in range(G.shape[0]):
        for j in range(G.shape[1]):

            for k in range(len(core_set)):
                for l in range(len(core_set)):

                    G[i,j] += D_core[l,k]*(VV_core[i*len(core_set) + k,
                        j*len(core_set) + l ] -  0.5*VV_core[i*len(core_set) + k,
                        l*len(core_set) + j ] )



    ehf = 0
    for i in range(len(core_set)):
        for j in range(len(core_set)):
            ehf += 0.5*D_core[i,j]*(2*h1_core[i,j] + G[i,j])

    print "Energy Core with coulomb repulsion and exchange ", ehf/K_L
    print 0.5*np.trace(np.dot(D_core, 2*h1_core + G))/K_L

    D2c = np.kron(D_core*0.5, D_core*0.5)

    U, D2ab_sbas, D2ab_sbas_rev, D2ab_abas, D2ab_abas_rev, D2ab_bas, D2ab_bas_rev = SpinAdapt(len(core_set)**2, len(core_set))

    K2_core = BuildK2( len(core_set), h1_core, VV_core, np.trace(D_core) )

    D2cSA = np.dot(U.T, np.dot(D2c, U))
    K2cSA = np.dot(U.T, np.dot(K2_core, U))
    
    K2cA = K2cSA[:len(D2ab_abas),:len(D2ab_abas)]
    K2cS = K2cSA[len(D2ab_abas):,len(D2ab_abas):]
    D2cA = D2cSA[:len(D2ab_abas),:len(D2ab_abas)]
    D2cS = D2cSA[len(D2ab_abas):,len(D2ab_abas):]

    ehf = np.trace(np.dot(3*K2cA, D2cA))
    ehf += np.trace(np.dot(K2cS, D2cS))

    print "K2 of core HF ", ehf/K_L

    tehf = np.sum(2*np.diag(h1_core)) 
    for i in range(h1_core.shape[0]):
        for j in range(h1_core.shape[0]):
            tehf += (2*VV_core[i*len(core_set) + j, i*len(core_set) + j] -
                    VV_core[i*len(core_set) + j, j*len(core_set) + i]  )

    print "Diag rep of core energy ", tehf/K_L

    return D_core , ehf/K_L

def GetEnergy(D2c, K2, r_dim):


    U, D2ab_sbas, D2ab_sbas_rev, D2ab_abas, D2ab_abas_rev, D2ab_bas, D2ab_bas_rev = SpinAdapt(r_dim**2, r_dim)

    D2cSA = np.dot(U.T, np.dot(D2c, U))
    K2cSA = np.dot(U.T, np.dot(K2, U))
    
    K2cA = K2cSA[:len(D2ab_abas),:len(D2ab_abas)]
    K2cS = K2cSA[len(D2ab_abas):,len(D2ab_abas):]
    D2cA = D2cSA[:len(D2ab_abas),:len(D2ab_abas)]
    D2cS = D2cSA[len(D2ab_abas):,len(D2ab_abas):]

    ehf = np.trace(np.dot(3*K2cA, D2cA))
    ehf += np.trace(np.dot(K2cS, D2cS))

    return ehf




if __name__=="__main__":

    os.chdir(sys.argv[1])

    sys.path.append(os.getcwd())

    ##############################
    #  
    #  Find the appropriate files
    #
    ##############################
    ifile = "./Integrals.dat"
    ofile = min(glob("*.o"), key=len)
    print ofile

    #############################
    #
    #  Get basks_rank and K_L
    #
    ############################
    with open(ofile,"r") as fid:
        text = fid.read()
        
        p = re.search("(basis rank =)\s+\w+", text)
        basis_rank = int(p.group().split('=')[1])
        p = re.search("(K_L =)\s+\w+", text)
        K_L = int(p.group().split('=')[1])
        nao = basis_rank
        p = re.search("(Nuclear repulsion energy =)\s+[0-9.-]+", text)
        enuc = float(p.group().split('=')[1])
        print "basis_rank ", basis_rank
        print "enuc ", enuc

    ##############################
    #
    #  Build H1 and V2
    #
    ##############################
    files = os.listdir('./')
    h1 = np.zeros((nao*K_L, nao*K_L),dtype=complex)
    VV_dict = {}
    vvrow = []
    vvcol = []
    vvdat = []
    for line in open(ifile):

        line = map(lambda x: float(x), line.strip().split('\t'))

        if line[2] == 0 and line[3] == 0:
            if line[0] == line[1]:
                h1[int(line[0])-1, int(line[1])-1] = line[4]+1j*line[5]
            else:
                h1[int(line[0])-1, int(line[1])-1] = line[4]+1j*line[5]
                h1[int(line[1])-1, int(line[0])-1] = line[4]-1j*line[5]
        else:
            vvrow.append( (int(line[0])-1)*nao*K_L + (int(line[1]) -1) )
            vvcol.append( (int(line[2])-1)*nao*K_L + (int(line[3]) -1) )
            vvdat.append( line[4] + 1j*line[5] )

    VV = csr_matrix((np.array(vvdat),(np.array(vvrow), np.array(vvcol))), 
            dtype=complex, shape=( (nao*K_L)**2, (nao*K_L)**2 ) )


    #Figure out a way to parse the KMO.rdm files
    RDM = np.zeros((nao,nao))
    for i in range(num_el/2):
        RDM[i,i] = 1.0

    print np.trace(RDM)

    DMO = block_diag(RDM,RDM)
    for i in range(2,K_L):
        DMO = block_diag(DMO,RDM)

    print np.trace(DMO)

    print "populating Hamiltonian and density matrix "
    new_order, old_order, new_bas = blockV2(K_L, nao)
    K2_list = [] 
    D2_list = []
    for i in range(K_L):

        #populate: row, col, dat list for each k-block
        k2_tmp = np.zeros((nao*nao*K_L, nao*nao*K_L),dtype=complex)
        d2_tmp = np.zeros((nao*nao*K_L, nao*nao*K_L),dtype=complex)
        for x in range(len(new_bas[i])):
            for y in range(len(new_bas[i])):

                ii,jj = new_bas[i][x]
                kk,ll = new_bas[i][y]
                d2_tmp[x,y] = DMO[ii,kk]*DMO[jj,ll]
                if jj == ll:
                    k2_tmp[x,y] += (1./(num_el*K_L - 1))*h1[ii,kk]
                if kk == ii:
                    k2_tmp[x,y] += (1./(num_el*K_L - 1))*h1[jj,ll]
                
                k2_tmp[x,y] += VV[ii*nao*K_L + jj, kk*nao*K_L + ll]


        #test how hermetian our matrix is?
        try:
            assert( np.abs(np.linalg.norm(k2_tmp - k2_tmp.conjugate().T)) <
                    float(1.0E-10) )
        except AssertionError:
            print "Assertion error"
            print "Norm should be zero"
            print np.linalg.norm(k2_tmp - k2_tmp.conjugate().T)

        K2_list.append( k2_tmp )
        D2_list.append( d2_tmp )
  
    print "built K2 and D2 blocks"
        
    #build symmetry adapting matrix for each block
    U_list = []
    D2A_list = []
    D2S_list = [] 
    K2A_list = []
    K2S_list = []
    for i in range(K_L):
        tmp = np.zeros((nao*nao*K_L, nao*nao*K_L))
        r_dim = len(new_bas[i])
        bas_rev = dict(zip(new_bas[i], range(len(new_bas[i])) ))
        #populate columns of spin adapting matrix
        D2ab_abas = {}
        D2ab_abas_rev = {}
        cnt = 0
        for xx in range(r_dim):
            if new_bas[i][xx][0] < new_bas[i][xx][1]:
                D2ab_abas[cnt] = new_bas[i][xx]
                D2ab_abas_rev[tuple(new_bas[i][xx])] = cnt
                cnt += 1

        D2ab_sbas = {}
        D2ab_sbas_rev = {}
        cnt = 0
        for xx in range(r_dim):
            if new_bas[i][xx][0] <= new_bas[i][xx][1]:
                D2ab_sbas[cnt] = new_bas[i][xx]
                D2ab_sbas_rev[tuple(new_bas[i][xx])] = cnt
                cnt += 1

        cnt = 0 
        for x in D2ab_abas.keys():
            a,b = D2ab_abas[x]
            x1 = bas_rev[(a,b)]
            x2 = bas_rev[(b,a)]
            tmp[x1,cnt] = 1.0/np.sqrt(2)
            tmp[x2,cnt] = -1.0/np.sqrt(2)
            cnt += 1
    
        for x in D2ab_sbas.keys():

            a,b = D2ab_sbas[x]
            x1 = bas_rev[(a,b)]
            x2 = bas_rev[(b,a)]
            
            if x1 == x2:
                tmp[x1, cnt ] = 1.0
            else:
                tmp[x1, cnt ] = 1./np.sqrt(2)
                tmp[x2, cnt ] = 1./np.sqrt(2)

            cnt += 1

        D2A_list.append( np.dot(tmp.T, np.dot( D2_list[i], tmp)
            )[:len(D2ab_abas),:len(D2ab_abas)]  )
        D2S_list.append( np.dot(tmp.T, np.dot( D2_list[i], tmp)
            )[len(D2ab_abas):,len(D2ab_abas):]  )
        K2A_list.append( np.dot(tmp.T, np.dot( K2_list[i], tmp)
            )[:len(D2ab_abas),:len(D2ab_abas)]  )
        K2S_list.append( np.dot(tmp.T, np.dot( K2_list[i], tmp)
            )[len(D2ab_abas):,len(D2ab_abas):]  )

        U_list.append(tmp)

    ehf = 0
    for i in range(K_L):
        ehf += np.trace(np.dot(K2A_list[i]*3, D2A_list[i]))
        ehf += np.trace(np.dot(K2S_list[i], D2S_list[i]))

    print "Hartree-Fock prepartitioning ", ehf/K_L + enuc

    ##################################
    #     
    #     Find Basis and divide into
    #         Core and Active
    #
    ##################################


    #build MO basis
    kvec = map(lambda x: 2*np.pi*x/K_L, range(K_L))
    kveci = range(K_L)
    basidx = {}
    for i in range(nao*K_L):
        basidx[i] = (kveci[i/nao], i%nao + 1)

    #Define core, active, virtual.  imported from CAVprep_input
    if len(core_set) + len(active_set) + len(virtual_set) != len(basidx):
        #throw error:
        error_string = "ERROR: size of core_set + active_set + virtual_set does not equal size"
        error_string += " of one-particle basis! redefine sets please "
        print error_string
        sys.exit()

    if len(core_set) == 0 and len(virtual_set) == 0:
        #Don't do anything...just write integrals to file
        np.save("D_HF_active.npy", DMO)
        np.save("h1_valence.npy", h1)
        np.save("V2_active.npy", VV.todense())
        K2 = BuildK2(nao*K_L, h1, VV, num_el*K_L)
        print GetEnergy(np.kron(DMO,DMO), K2, nao*K_L)/K_L + enuc
        sys.exit()
        
        

    if len(core_set) > 0:

        ##################################
        #     
        #     Core Component
        #
        ##################################

        D_core = DMO[core_set,:]
        D_core = D_core[:,core_set]
        h1_core = h1[core_set,:]
        h1_core = h1_core[:,core_set]

        #Construct 2-electron Core set
        VV_core = np.zeros((len(core_set)**2, len(core_set)**2), dtype=complex)
        for i in range(len(core_set)):
            for j in range(len(core_set)):
                for k in range(len(core_set)):
                    for l in range(len(core_set)):

                        a = core_set[i]
                        b = core_set[j]
                        p = core_set[k]
                        q = core_set[l]

                        VV_core[i*len(core_set)+j, k*len(core_set)+l] =  VV[a*nao*K_L + b, p*nao*K_L + q ]

        D2c = np.kron(D_core, D_core)
        D2c_bas = list(product(core_set, core_set))
        K2_core = BuildK2( len(core_set), h1_core, VV_core, 2*np.trace(D_core) )
        EHF_Core = GetEnergy(D2c, K2_core, len(core_set))
        print "Core Energy ", EHF_Core/K_L

    if len(active_set) > 0:

        ##################################
        #     
        #     active Component
        #
        ##################################

        D_active = DMO[active_set,:]
        D_active = D_active[:,active_set]
        h1_active = h1[active_set,:]
        h1_active = h1_active[:,active_set]

        #Construct 2-electron Outer Set
        VV_active = np.zeros((len(active_set)**2, len(active_set)**2), dtype=complex)
        for i in range(len(active_set)):
            for j in range(len(active_set)):
                for k in range(len(active_set)):
                    for l in range(len(active_set)):

                        a = active_set[i]
                        b = active_set[j]
                        p = active_set[k]
                        q = active_set[l]

                        VV_active[i*len(active_set)+j, k*len(active_set)+l] =  VV[a*nao*K_L + b, p*nao*K_L + q ]


        D2a = np.kron(D_active, D_active)
        np.save("D_HF_active",D_active)
        Vac = np.zeros_like(h1_active)
        for i in range(len(active_set)):
            for j in range(len(core_set)):
                #add repuslive field from core set
                for k in range(len(active_set)):
                    for l in range(len(core_set)):
                        
                        i_idx = active_set[i]
                        j_idx = core_set[j]
                        k_idx = active_set[k]
                        l_idx = core_set[l]
                        
                        Vac[i,k] += 2*D_core[l,j]*(VV[i_idx*nao*K_L + j_idx,
                                k_idx*nao*K_L + l_idx] - 0.5*VV[i_idx*nao*K_L +
                                    j_idx, l_idx*nao*K_L + k_idx] )

        h1_valence = h1_active + Vac
        K2_valencemod = BuildK2( len(active_set), h1_valence, VV_active,
                np.trace(D_active)*2 )
        EHF_valencemod = GetEnergy(D2a, K2_valencemod, len(active_set))

        np.save("h1_valence", h1_valence)
        np.save("V2_active", VV_active)
        np.save("K2_active", K2_valencemod)
    
    try:
        print "Core-Valencemod ", EHF_Core/K_L + EHF_valencemod/K_L
        with open("Core_Energy.dat", "w") as fid:
            fid.write("%20.20f\n"%(EHF_Core/K_L).real)
            fid.write("h1shape = %i\n"%h1_valence.shape[0])
            fid.write("VVshape = %i\n"%VV_active.shape[0])
    except:
        print "no core energy"



    if len(virtual_set) > 0:

        ##################################
        #     
        #     Virtual Component
        #
        ##################################

        #Nothing to do since no electrons should occupy that space.  Check if there
        #are any electrons that are in that space.  If so throw an error
        for i in virtual_set:
            if DMO[i,i] != 0.0:
                print "Found occupation in vitual orbitaL"
                print "redefine virtual to have zero occupation at Hartree-Fock Level"
                sys.exit()


    print "Writing new EI file. "

    #Count number of non-zero two-electron integrals
    cnt = 0
    for i in range(VV_active.shape[0]):
        for j in range(VV_active.shape[0]):

            if np.linalg.norm(VV_active[i,j]) != 0.0 +1j*0.0:
                cnt += 1

    with open("Integrals2.dat","w") as fid:

        fid.write("%i\t%i\t1.0e-05\t%i\t0\n"%(np.trace(D_active),
            len(active_set), cnt ))

        for i in range(h1_valence.shape[0]):
            for j in range(i,h1_valence.shape[1]):
                fid.write("%i\t%i\t0\t0\t%20.14f\t%20.14f\n"%(i+1,j+1,
                    h1_valence[i,j].real, h1_valence[i,j].imag) )

        cnt2 = 0
        for i in range(len(active_set)):
            for j in range(len(active_set)):
                for k in range(len(active_set)):
                    for l in range(len(active_set)):

                        if np.linalg.norm(VV_active[i*len(active_set) + j,
                            k*len(active_set) + l] ) != 0.0 + j*0.0:
                        #if np.linalg.norm(VV_active[i*len(active_set) + j,
                        #    k*len(active_set) + l] ) < float(1.0E-13):
 
                            fid.write("%i\t%i\t%i\t%i\t%20.14f\t%20.14f\n"%(i+1, j+1, k+1,
                                l+1, VV_active[i*len(active_set) +
                                    j,k*len(active_set)+l].real ,
                                VV_active[i*len(active_set) +
                                    j,k*len(active_set)+l].imag
                                ) )
                            cnt2 += 1

