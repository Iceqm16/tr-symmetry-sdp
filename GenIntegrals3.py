'''
Generate ei file for Integrals
'''

import numpy as np
import sys

if __name__=="__main__":


    M = int(sys.argv[2])  
    K_L = int(sys.argv[1])
    num_el = int(sys.argv[3])
    rdim = M*K_L
    #H1 = np.zeros((rdim, rdim),dtype=float)
    #V1 = np.zeros((rdim**2, rdim**2),dtype=float)

    
    cnt = 0
    lines = []
    for line in open("./Integrals.dat"):
        #line = map(float, line.split('\t'))
        if '0\t0\t' in line:
            print line
        else:
            cnt += 1
        lines.append(line)

    print "finished file read" 
    with open("./Integrals2.ei",'w') as fid:

        fid.write("%i\t%i\t1e-05\t%i\t0\n"%( num_el*K_L/2, M*K_L, cnt) )
        for line in lines:
            fid.write(line)

