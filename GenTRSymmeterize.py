'''
Make the ei file from the Integrals.dat file and generate a makeSDP
'''

import sys
import os
from glob import glob
import re

def writePBSTRsym(runname, RDMlevel,nodes,ppn,cnt):


    runname += "TRsym"
    Text = '###PBS submission Script\n\n'
    INPUT = '#PBS -N ' + runname + '\n'
    INPUT = INPUT + '#PBS -e ' + runname + '.e' + '\n'
    INPUT = INPUT + '#PBS -o ' + runname + '.o' + '\n'
    INPUT = INPUT + '#PBS -l nodes=%i:ppn=%i\n\n' % (nodes,ppn)

    runinfo = "module load epd\n"
    run = 'python /home/nick/pyncr/postprocess/PBC/pybin/ActiveSpace/TRSymmetry.py'

    setEnv = 'SCR_PATH=\"/state/partition1/scr/nick\"\n\n'
    setEnv = setEnv + 'if [ ! -e $SCR_PATH ]; then\nmkdir -p $SCR_PATH\nfi\n\n'
    setEnv = setEnv + 'cd $PBS_O_WORKDIR\n\n'

    
    Text = Text + INPUT + runinfo + setEnv + run

    fid = open(runname+'.pbs','w')
    fid.write(Text)
    fid.close() 




if __name__=="__main__":

    pwd = os.getcwd()

    if len(sys.argv) != 2:
        print "first input is directory"
        sys.exit()

    os.chdir(sys.argv[1])

    dirs = filter(os.path.isdir, os.listdir('./'))
    print dirs
    cnt = 0
    for dd in dirs:

        os.chdir(dd)
        #write makeSDP
        writePBSTRsym(dd,'DQG',1,1,cnt)

        #open file and write eifile
        cnt += 1
        os.chdir("../")
        
