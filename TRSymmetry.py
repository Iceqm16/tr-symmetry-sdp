"""
Generate Time-Reversal symmetric Integral.dat file.  For generating a
time-reversal symmetric Hamiltonian

"""

import numpy as np
import sys
import os
from glob import glob
import re
from itertools import product
from scipy.sparse import csr_matrix
from scipy.linalg import block_diag


K_L = 4
nao = 2

def Ints2tensors(Ints):

    #find basis rank
    cnt = 0
    while(Ints[cnt, 2] == 0):
        cnt += 1
    rdim = int(Ints[cnt - 1, 0])
    indices = np.triu_indices(rdim)
    h1 = np.zeros((rdim,rdim),dtype=complex)
    h1[indices] = Ints[:cnt,4] + 1j*Ints[:cnt,5]
    h1 = h1 + h1.conjugate().T
    h1[np.diag_indices_from(h1)] /= 2


    h1_2 = np.zeros((rdim, rdim), dtype=complex)
    v2_row = []
    v2_col = []
    v2_dat = []
    cnt = 0
    while(int(Ints[cnt, 2 ]) == 0):
        i,j = int(Ints[cnt,0]), int(Ints[cnt,1])
        if i == j:
            h1_2[i -1 , j - 1] = Ints[cnt, 4] 
        else:
            h1_2[i -1 , j - 1] = Ints[cnt, 4] + 1j*Ints[cnt,5]
            h1_2[j -1 , i - 1] = Ints[cnt, 4] - 1j*Ints[cnt,5]
        cnt += 1


    while(True):

        try:
            i,j = int(Ints[cnt,0])-1, int(Ints[cnt,1])-1
            k,l = int(Ints[cnt,2])-1, int(Ints[cnt,3])-1
        except IndexError:
            break

        if i*rdim + j == k*rdim + l:
            v2_row.append(i*rdim + j)
            v2_col.append(k*rdim + l)
            v2_dat.append(Ints[cnt,4] ) # + 1j*Ints[cnt,5])
            #positive semidefinite matrices have real valued diagonals!!!!!
        else:
            v2_row.append(i*rdim + j)
            v2_col.append(k*rdim + l)
            v2_dat.append(Ints[cnt,4] + 1j*Ints[cnt,5])
            v2_row.append(k*rdim + l)
            v2_col.append(i*rdim + j)
            v2_dat.append(Ints[cnt,4] - 1j*Ints[cnt,5])

        cnt += 1
  
    VV = csr_matrix((v2_dat, (v2_row, v2_col)),shape=(rdim*rdim, rdim*rdim))
    
    return h1_2, VV, rdim


def Kblock(VV,H1):

    #make k-space basis
    bas = {}
    cnt = 0
    for k in range(K_L):
        for n in range(nao):
            bas[cnt] = (k,n)
            cnt += 1
    bas_rev = dict(zip(bas.values(), bas.keys()))

    d2bas = dict(zip(range(nao*K_L*nao*K_L), product(range(nao*K_L),
        range(nao*K_L))))

    Kd2bas = []
    new_order = []
    for kk in range(K_L):
        tmpbas = {}
        gem_cnt = 0
        for idx, gem in d2bas.iteritems():
            i,j = gem
            ki,ni = bas[i]
            kj,nj = bas[j]
            if (kj + ki)%K_L == kk:
                tmpbas[gem_cnt] = gem
                new_order.append(idx)
                gem_cnt += 1

        Kd2bas.append(tmpbas)

    KVV2 = VV.todense()
    KVV2 = KVV2[:,new_order]
    KVV2 = KVV2[new_order,:]

    KV2 = []
    for kk in range(K_L):
        KV2.append(KVV2[kk*(nao*nao*K_L):(kk+1)*(nao*nao*K_L),kk*(nao*nao*K_L):(kk+1)*(nao*nao*K_L)])
        
   
    #test if we ordered correctly
    blocked_KV2 = block_diag(KV2[0], KV2[1])
    for i in range(2,len(KV2)):
        blocked_KV2 = block_diag(blocked_KV2, KV2[i])
    for x in range(KVV2.shape[0]):
        for y in range(KVV2.shape[1]):
            assert(KVV2[x,y] - blocked_KV2[x,y] == 0)
    
    #Successfully made it through assertion...we have blocked out our matrix
    assert(np.linalg.norm(KVV2 - blocked_KV2) == 0.0)
    
    #time-reversal forces blocks with zero momentum and pi momentum to have no
    #imaginary component
    KV2[0].imag = np.zeros_like(KV2[0].real)
    KV2[K_L/2].imag = np.zeros_like(KV2[K_L/2].real)

    #iterate over basis for k
    new_order_set = []
    tmp_bas_set = {}
    cnt = 0
    for kk in range(K_L/2):
        #find Kd2bas that has momentum != 0/pi
        if kk == 0 or kk == K_L/2:
            pass
        else:
            new_order = []
            tmp_bas = {}
            cnt = 0
            for idx, gem in Kd2bas[kk].iteritems():

                i,j = gem
                ki,ni = bas[i]
                kj,nj = bas[j]

                #now find (-ki,ni) (-kj, nj)
                for idx2, gem2 in Kd2bas[-kk].iteritems():

                    r,s = gem2
                    kr,nr = bas[r]
                    ks,ns = bas[s]

                    if (-ki%K_L,ni) == (kr,nr) and (-kj%K_L, nj) == (ks,ns):
                        new_order.append(idx2)
                        tmp_bas[cnt] = gem2
                        cnt += 1

            assert(len(new_order) == nao*nao*K_L) 


            #reorder the matrix and assign new basis ordering.
            KV2[-kk] = KV2[-kk][:,new_order]
            KV2[-kk] = KV2[-kk][new_order,:]
            Kd2bas[-kk] = tmp_bas
            KV2[-kk] = np.copy(KV2[kk].conjugate())
            try:
                assert(np.linalg.norm(KV2[kk] - KV2[-kk].conjugate()) < float(1.0E-12))
            except AssertionError:
                print "Time-Reversal symmetry rectifies "
                print "for (k,-k) ", (kk, (-kk)%K_L)
                print np.linalg.norm(KV2[kk] - KV2[-kk].conjugate())
                print np.max(np.abs(KV2[kk] - KV2[-kk].conjugate()))

    
    
    #write new Integrals.dat
    with open("Integrals.dat","w") as fid:

        for x in range(H1.shape[0]):
            for y in range(x,H1.shape[-1]):
                fid.write("%i\t%i\t0\t0\t%4.10E\t%4.10E\n"%(x+1,y+1,
                    H1[x,y].real, H1[x,y].imag))

        for kk in range(K_L):
            
            print kk
            print KV2[kk].shape
            for idx, gem in Kd2bas[kk].iteritems():
                for idx2, gem2 in Kd2bas[kk].iteritems():

                    if idx2 >= idx and np.linalg.norm(KV2[kk][idx,idx2]) > float(1.0E-11):
                        fid.write("%i\t%i\t%i\t%i\t%4.15E\t%4.15E\n"%(gem[0]+1,
                            gem[1]+1,
                            gem2[0]+1,
                            gem2[1]+1,
                            KV2[kk][idx,idx2].real,
                            KV2[kk][idx,idx2].imag))



        

if __name__=="__main__":

    #Get .o file parameters
    ofile = min(glob("*.o"),key=len)
    with open(ofile,"r") as fid:
        text = fid.read()
        p = re.search("(basis rank =)\s+\w+", text)
        basis_rank = int(p.group().split('=')[1])
        p = re.search("(K_L =)\s+\w+", text)
        K_L = int(p.group().split('=')[1])
        nao = basis_rank
        p = re.search("(Nuclear repulsion energy =)\s+[0-9.-]+", text)
        enuc = float(p.group().split('=')[1])
 
    #Do stuff to integrals
    Integrals = np.loadtxt("./Integrals.dat")
    H1, VV, rdim = Ints2tensors(Integrals)
    VVK = Kblock(VV,H1)



