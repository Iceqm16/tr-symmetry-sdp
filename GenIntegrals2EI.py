'''
Make the ei file from the Integrals.dat file and generate a makeSDP
'''

import sys
import os
from glob import glob
import re

def writePBS(runname, nodes,ppn, K_L, nao, num_el, cnt):

    #node_list = ['sequoia-0-0', 'sequoia-0-1', 'sequoia-0-2', 'sequoia-0-3']
    #print runname 
    runname += "genEI"
    Text = '###PBS submission Script\n\n'
    INPUT = '#PBS -N ' + runname + '\n'
    INPUT = INPUT + '#PBS -e ' + runname + '.e' + '\n'
    INPUT = INPUT + '#PBS -o ' + runname + '.o' + '\n'
    INPUT = INPUT + '#PBS -l nodes=%i:ppn=%i\n\n' % (nodes,ppn)
    #INPUT = INPUT + '#PBS -l nodes=%s:ppn=%i\n\n' % (node_list[cnt%4] ,ppn)



    runinfo = 'module load epd\n'    
    run = 'python /home/nick/pyncr/postprocess/PBC/pybin/GenIntegrals3.py %i %i %i'%( K_L, nao, num_el)
    

    setEnv = 'SCR_PATH=\"/state/partition1/scr/nick\"\n\n'
    setEnv = setEnv + 'if [ ! -e $SCR_PATH ]; then\nmkdir -p $SCR_PATH\nfi\n\n'
    setEnv = setEnv + 'cd $PBS_O_WORKDIR\n\n'

    
    Text = Text + INPUT + runinfo + setEnv + run

    fid = open(runname+'.pbs','w')
    fid.write(Text)
    fid.close() 



if __name__=="__main__":

    pwd = os.getcwd()

    if len(sys.argv) != 5:
        print "ERROR: NOT ENOUGH INPUT"
        sys.exit()

    os.chdir(sys.argv[1])
    K_L = int(sys.argv[2])
    nao = int(sys.argv[3])
    num_el = int(sys.argv[4])


    dirs = filter(os.path.isdir, os.listdir('./'))
    
    cnt = 0
    for dd in dirs:

        os.chdir(dd)
        print os.getcwd()
        
        writePBS(dd,1,1, K_L, nao, num_el, cnt)
        cnt +=1
        os.chdir("../")
        
