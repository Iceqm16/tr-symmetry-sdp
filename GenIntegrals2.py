'''
Generate ei file for Integrals
'''

import numpy as np
import sys

if __name__=="__main__":


    M = int(sys.argv[2])  
    K_L = int(sys.argv[1])
    num_el = int(sys.argv[3])
    rdim = M*K_L
    H1 = np.zeros((rdim, rdim),dtype=float)
    V1 = np.zeros((rdim**2, rdim**2),dtype=float)


    cnt = 0
    for line in open("./Integrals.dat"):
        line = map(float, line.split('\t'))
        if line[2] == 0.0 and line[3] == 0.0:
            H1[int(line[0]) - 1, int(line[1]) - 1] = line[4] 

        else:
            V1[ ( int(line[0]) - 1 )*rdim + (int(line[1]) - 1) , 
                ( int(line[2]) - 1 )*rdim + (int(line[3]) - 1) ] = line[4] 
            cnt += 1
            
            

    
    with open("./Integrals.ei",'w') as fid:

        fid.write("%i\t%i\t1e-05\t%i\t0\n"%( num_el*K_L/2, M*K_L, cnt) )
        for x in range(rdim):
            for y in range(x,rdim):
                fid.write("%i\t%i\t0\t0\t%4.10f\n"%(x+1, y+1, H1[x,y].real))

        cnt = 0 
        for i in range(rdim):
            for j in range(rdim):
                for k in range(rdim):
                    for l in range(rdim):

                        if (np.abs(V1[i*rdim + j, k*rdim +l]) >
                                float(1.0E-9) ) :

                            fid.write("%i\t%i\t%i\t%i\t%4.15f\n"%(i+1,j+1,k+1,l+1,
                                V1[i*rdim + j, k*rdim+l] ) ) 

                            cnt += 1
        print cnt
