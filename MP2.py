"""

determines MP2 energy correction for momentum-space system


"""

import numpy as np
import sys
import os
from glob import glob
import re
from itertools import product
from scipy.sparse import csr_matrix, coo_matrix
from scipy.linalg import block_diag


def GetEnergy(D2c, K2, r_dim):


    U, D2ab_sbas, D2ab_sbas_rev, D2ab_abas, D2ab_abas_rev, D2ab_bas, D2ab_bas_rev = SpinAdapt(r_dim**2, r_dim)

    D2cSA = np.dot(U.T, np.dot(D2c, U))
    K2cSA = np.dot(U.T, np.dot(K2, U))
    
    K2cA = K2cSA[:len(D2ab_abas),:len(D2ab_abas)]
    K2cS = K2cSA[len(D2ab_abas):,len(D2ab_abas):]
    D2cA = D2cSA[:len(D2ab_abas),:len(D2ab_abas)]
    D2cS = D2cSA[len(D2ab_abas):,len(D2ab_abas):]

    ehf = np.trace(np.dot(3*K2cA, D2cA))
    ehf += np.trace(np.dot(K2cS, D2cS))

    return ehf



def SpinAdapt(gem_dim, r_dim):

    bas = dict(zip(range(gem_dim), product(range(1,r_dim+1),
        range(1,r_dim+1))))
    bas_rev = dict(zip( bas.values(), bas.keys()))

    #prepare for spin adapt
    D2ab_abas = {}
    D2ab_abas_rev = {}
    cnt = 0
    for xx in range(r_dim):
        for yy in range(xx+1,r_dim):
            D2ab_abas[cnt] = (xx+1, yy+1)
            D2ab_abas_rev[(xx+1, yy+1)] = cnt
            cnt += 1

    D2ab_sbas = {}
    D2ab_sbas_rev = {}
    cnt = 0
    for xx in range(r_dim):
        for yy in range(xx,r_dim):
            D2ab_sbas[cnt] = (xx+1, yy+1)
            D2ab_sbas_rev[(xx+1, yy+1)] = cnt
            cnt += 1

    
    U = np.zeros((gem_dim,gem_dim))
    cnt = 0
    for xx in D2ab_abas.keys():
        i,j = D2ab_abas[xx]
        x1 = bas_rev[(i,j)]
        x2 = bas_rev[(j,i)]
        U[x1, cnt ] = 1./np.sqrt(2)
        U[x2, cnt ] = -1./np.sqrt(2)
        cnt += 1

    for xx in D2ab_sbas.keys():

        i,j = D2ab_sbas[xx]
        x1 = bas_rev[(i,j)]
        x2 = bas_rev[(j,i)]
        
        if x1 == x2:
            U[x1, cnt ] = 1.0
        else:
            U[x1, cnt ] = 1./np.sqrt(2)
            U[x2, cnt ] = 1./np.sqrt(2)

        cnt += 1
    
    return U, D2ab_sbas, D2ab_sbas_rev, D2ab_abas, D2ab_abas_rev, bas, bas_rev


def BasOrder(basfile, D2ab_abas, D2ab_sbas):

  
    D2Abas, D2Sbas, D1bas, G2bas = RDMUtil.getBas(basfile)

    D2Abas_rev = map(lambda x: dict(zip(D2Abas[x].values(), D2Abas[x].keys())), range(len(D2Abas)) )
    D2Sbas_rev = map(lambda x: dict(zip(D2Sbas[x].values(), D2Sbas[x].keys())) , range(len(D2Sbas)) )
    D2ab_abas_rev = dict(zip(D2ab_abas.values(), D2ab_abas.keys()))
    D2ab_sbas_rev = dict(zip(D2ab_sbas.values(), D2ab_sbas.keys()))
 


    D2A_new_order = []
    for i in range(len(D2Abas)):
        for j in range(len(D2Abas[i])):
            D2A_new_order.append(D2ab_abas_rev[ D2Abas[i][j] ])
    D2S_new_order = []
    for i in range(len(D2Sbas)):
        for j in range(len(D2Sbas[i])):
            D2S_new_order.append(D2ab_sbas_rev[ D2Sbas[i][j] ])


    return D2A_new_order, D2S_new_order, D2Abas, D2Sbas, D1bas, G2bas



def Ints2tensors(Ints):

    #find basis rank
    cnt = 0
    while(Ints[cnt, 2] == 0):
        cnt += 1
    rdim = int(Ints[cnt - 1, 0])
    indices = np.triu_indices(rdim)
    h1 = np.zeros((rdim,rdim),dtype=complex)
    h1[indices] = Ints[:cnt,4] + 1j*Ints[:cnt,5]
    h1 = h1 + h1.conjugate().T
    h1[np.diag_indices_from(h1)] /= 2


    h1_2 = np.zeros((rdim, rdim), dtype=complex)
    v2_row = []
    v2_col = []
    v2_dat = []
    cnt = 0
    while(int(Ints[cnt, 2 ]) == 0):
        i,j = int(Ints[cnt,0]), int(Ints[cnt,1])
        if i == j:
            h1_2[i -1 , j - 1] = Ints[cnt, 4] 
        else:
            h1_2[i -1 , j - 1] = Ints[cnt, 4] + 1j*Ints[cnt,5]
            h1_2[j -1 , i - 1] = Ints[cnt, 4] - 1j*Ints[cnt,5]
        cnt += 1


    while(True):

        try:
            i,j = int(Ints[cnt,0])-1, int(Ints[cnt,1])-1
            k,l = int(Ints[cnt,2])-1, int(Ints[cnt,3])-1
        except IndexError:
            break

        if i*rdim + j == k*rdim + l:
            v2_row.append(i*rdim + j)
            v2_col.append(k*rdim + l)
            v2_dat.append(Ints[cnt,4] ) # + 1j*Ints[cnt,5])
            #positive semidefinite matrices have real valued diagonals!!!!!
        else:
            v2_row.append(i*rdim + j)
            v2_col.append(k*rdim + l)
            v2_dat.append(Ints[cnt,4] + 1j*Ints[cnt,5])
            v2_row.append(k*rdim + l)
            v2_col.append(i*rdim + j)
            v2_dat.append(Ints[cnt,4] - 1j*Ints[cnt,5])

        cnt += 1
  
    VV = csr_matrix((v2_dat, (v2_row, v2_col)),shape=(rdim*rdim, rdim*rdim))
    
    return h1_2, VV, rdim


def Kblock(VV,H1):

    #make k-space basis
    bas = {}
    cnt = 0
    for k in range(K_L):
        for n in range(nao):
            bas[cnt] = (k,n)
            cnt += 1
    bas_rev = dict(zip(bas.values(), bas.keys()))

    d2bas = dict(zip(range(nao*K_L*nao*K_L), product(range(nao*K_L),
        range(nao*K_L))))

    Kd2bas = []
    new_order = []
    for kk in range(K_L):
        tmpbas = {}
        gem_cnt = 0
        for idx, gem in d2bas.iteritems():
            i,j = gem
            ki,ni = bas[i]
            kj,nj = bas[j]
            if (kj + ki)%K_L == kk:
                tmpbas[gem_cnt] = gem
                new_order.append(idx)
                gem_cnt += 1

        Kd2bas.append(tmpbas)

    KVV2 = VV.todense()
    KVV2 = KVV2[:,new_order]
    KVV2 = KVV2[new_order,:]

    KV2 = []
    for kk in range(K_L):
        KV2.append(KVV2[kk*(nao*nao*K_L):(kk+1)*(nao*nao*K_L),kk*(nao*nao*K_L):(kk+1)*(nao*nao*K_L)])
        
   
    #test if we ordered correctly
    blocked_KV2 = block_diag(KV2[0], KV2[1])
    for i in range(2,len(KV2)):
        blocked_KV2 = block_diag(blocked_KV2, KV2[i])
    for x in range(KVV2.shape[0]):
        for y in range(KVV2.shape[1]):
            assert(KVV2[x,y] - blocked_KV2[x,y] == 0)
    
    #Successfully made it through assertion...we have blocked out our matrix
    assert(np.linalg.norm(KVV2 - blocked_KV2) == 0.0)
    
    #time-reversal forces blocks with zero momentum and pi momentum to have no
    #imaginary component
    KV2[0].imag = np.zeros_like(KV2[0].real)
    KV2[K_L/2].imag = np.zeros_like(KV2[K_L/2].real)

    #iterate over basis for k
    new_order_set = []
    tmp_bas_set = {}
    cnt = 0
    for kk in range(K_L/2):
        #find Kd2bas that has momentum != 0/pi
        if kk == 0 or kk == K_L/2:
            pass
        else:
            new_order = []
            tmp_bas = {}
            cnt = 0
            for idx, gem in Kd2bas[kk].iteritems():

                i,j = gem
                ki,ni = bas[i]
                kj,nj = bas[j]

                #now find (-ki,ni) (-kj, nj)
                for idx2, gem2 in Kd2bas[-kk].iteritems():

                    r,s = gem2
                    kr,nr = bas[r]
                    ks,ns = bas[s]

                    if (-ki%K_L,ni) == (kr,nr) and (-kj%K_L, nj) == (ks,ns):
                        new_order.append(idx2)
                        tmp_bas[cnt] = gem2
                        cnt += 1

            assert(len(new_order) == nao*nao*K_L) 


            #reorder the matrix and assign new basis ordering.
            KV2[-kk] = KV2[-kk][:,new_order]
            KV2[-kk] = KV2[-kk][new_order,:]
            Kd2bas[-kk] = tmp_bas
            KV2[-kk] = np.copy(KV2[kk].conjugate())
            try:
                assert(np.linalg.norm(KV2[kk] - KV2[-kk].conjugate()) < float(1.0E-12))
            except AssertionError:
                print "Time-Reversal symmetry rectifies "
                print "for (k,-k) ", (kk, (-kk)%K_L)
                print np.linalg.norm(KV2[kk] - KV2[-kk].conjugate())
                print np.max(np.abs(KV2[kk] - KV2[-kk].conjugate()))

    
    
    #write new Integrals.dat
    with open("Integrals.dat","w") as fid:

        for x in range(H1.shape[0]):
            for y in range(x,H1.shape[-1]):
                fid.write("%i\t%i\t0\t0\t%4.10E\t%4.10E\n"%(x+1,y+1,
                    H1[x,y].real, H1[x,y].imag))

        for kk in range(K_L):
            
            print kk
            print KV2[kk].shape
            for idx, gem in Kd2bas[kk].iteritems():
                for idx2, gem2 in Kd2bas[kk].iteritems():

                    if idx2 >= idx and np.linalg.norm(KV2[kk][idx,idx2]) > float(1.0E-11):
                        fid.write("%i\t%i\t%i\t%i\t%4.15E\t%4.15E\n"%(gem[0]+1,
                            gem[1]+1,
                            gem2[0]+1,
                            gem2[1]+1,
                            KV2[kk][idx,idx2].real,
                            KV2[kk][idx,idx2].imag))

def BuildK2(r_dim, h1, VV, num_el):

    K2 = np.zeros(( (r_dim)**2, (r_dim)**2), dtype=complex)
    for i in range(r_dim):
        for j in range(r_dim):
            for k in range(r_dim):
                for l in range(r_dim):

                    if j == l:
                        K2[i*(r_dim)+j, k*(r_dim)+l] += (1./(num_el - 1))*(h1[i,k])
                    if i == k:
                        K2[i*(r_dim)+j, k*(r_dim)+l] += (1./(num_el - 1))*(h1[j,l])
                    
                    K2[i*(r_dim)+j, k*(r_dim)+l] += VV[i*(r_dim)+j, k*(r_dim)+l]

    return K2

def CreateHFD2(nao, K_L, num_el):

    tmp = np.zeros((nao,nao),dtype=complex)
    for i in range(num_el/2):
        tmp[i,i] = 1.0

    if K_L == 1:
        D1K = tmp
    elif K_L == 2:
        D1K = block_diag(tmp,tmp)
    else:
        D1K = tmp
        for i in range(1,K_L):
            D1K = block_diag(D1K, tmp)


    D2HF = np.zeros(( nao*nao*K_L*K_L, nao*nao*K_L*K_L ), dtype=complex)
    rdim = nao*K_L
    for i in range(rdim):
        for j in range(rdim):
            for k in range(rdim):
                for l in range(rdim):
                    #ironically this is equivalent to the kronecker product!
                    D2HF[i*rdim + j, k*rdim + l] =  0.5*(D1K[i,k]*D1K[j,l] 
                            + D1K[j, l]*D1K[i,k] - D1K[j,k]*D1K[i,l] -
                            D1K[i,l]*D1K[j,k])

    print "Trace of D2HF ", np.trace(np.kron(D1K,D1K))

    return np.kron(D1K,D1K), D1K

def CreateD1KD(nao, K_L, num_el):

    tmp = np.zeros((nao,nao),dtype=complex)
    for i in range(num_el/2):
        tmp[i,i] = 1.0

    if K_L == 1:
        D1K = tmp
    elif K_L == 2:
        D1K = block_diag(tmp,tmp)
    else:
        D1K = tmp
        for i in range(1,K_L):
            D1K = block_diag(D1K, tmp)

    return 2.0*D1K

if __name__=="__main__":

    os.chdir(sys.argv[1]) 
    #Get .o file parameters
    ofile = min(glob("*.o"),key=len)
    with open(ofile,"r") as fid:
        text = fid.read()
        p = re.search("(basis rank =)\s+\w+", text)
        basis_rank = int(p.group().split('=')[1])
        p = re.search("(K_L =)\s+\w+", text)
        K_L = int(p.group().split('=')[1])
        nao = basis_rank
        p = re.search("(Nuclear repulsion energy =)\s+[0-9.-]+", text)
        enuc = float(p.group().split('=')[1])
        p = re.search("(xD block AO Norm =)\s+[0-9.-]+", text)
        num_el = int(float(p.group().split('=')[1]))
        p = re.search("(Total Energy)\s+[0-9.-]+", text)
        HF_kspace_energy = float(p.group().split(' ')[-1])
 

 
    #Do stuff to integrals
    Integrals = np.loadtxt("./Integrals.dat")
    H1, VV, rdim = Ints2tensors(Integrals)
    rdim = nao*K_L


    #MP2 loops involve eigenvalues of Fock operator.  We need to generate
    #these.  
    D1KMO = CreateD1KD(nao, K_L, num_el)
    VV = VV.todense()

    Gmat = np.zeros_like(H1)
    #Gmat = np.einsum('ijkl,lj->ik',VV,D1KMO) #Coulomb
    #Gmat += np.einsum('ijlk,lj->ik',VV,-0.5*D1KMO) #exchange


    #Gmat_2 = np.zeros_like(H1)
 
    for i in range(rdim):
        for j in range(rdim):
            for k in range(rdim):
                for l in range(rdim):

                    Gmat[i,k] += D1KMO[l,j]*(VV[i*rdim + j, k*rdim + l] -
                            0.5*VV[i*rdim + j, l*rdim + k] )

    Fock = H1 + Gmat

    print (np.trace(np.dot(D1KMO, H1)) + np.trace(np.dot(0.5*D1KMO, Gmat)))/K_L + enuc
    print np.trace(np.dot(0.5*D1KMO, Fock + H1))/K_L + enuc


    #break up Fock operator into it's k-space componentes and get eigenvalues
    #of the orbitals
    FockK = []
    Eigs = []
    for k in range(K_L):
        FockK.append(Fock[k*nao:(k+1)*nao,k*nao:(k+1)*nao])
        w,v = np.linalg.eigh(FockK[-1])
        Eigs.append(w)

   
    #no we want to perform the loops.  loop over occupied set and virtual set
    #and for each of those loop over the three relevant k-points (look in MO
    #rotations to determine relevant k-points

    #generate basis for partitioning indices into occ and virt.
    bas = {}
    cnt = 0
    occ_set = []
    virt_set = []
    for k in range(K_L):
        for m in range(nao):
            bas[cnt] = (k,m)
            if m < num_el/2:
                occ_set.append(cnt)
            else:
                virt_set.append(cnt)
            cnt+= 1

    print occ_set
    print virt_set

    occ_loc = range(nao)[:num_el/2]
    virt_loc = range(nao)[num_el/2:]
    print occ_loc
    print virt_loc

    #EMP2 = 0
    #for i in occ_loc:
    #    for j in occ_loc:
    #        for a in virt_loc:
    #            for b in virt_loc:

    #                #loop over k-points
    #                for ki in range(K_L):
    #                    for ka in range(K_L):
    #                        for kb in range(K_L):

    #                            #get Hartree-Fock eigenvalue denominator
    #                            #occ pair - virtual pair
    #                            kj = (ka+kb-ki)%K_L
    #                            D = Eigs[ki][i] + Eigs[kj][j] - Eigs[ka][a] - Eigs[kb][b]

    #                            #get 2-electron integral numerator; indices --> integrals
    #                            ii = nao*ki + i
    #                            jj = nao*kj + j
    #                            aa = nao*ka + a
    #                            bb = nao*kb + b

    #                            numerator = 2.0*np.linalg.norm(VV[ii*rdim + jj,
    #                                aa*rdim+ bb]) -\
    #                                (VV[ii*rdim + jj, aa*rdim + bb]*VV[ii*rdim + jj, bb*rdim + aa]).real

    #                            EMP2 += numerator/D

    #print EMP2/K_L

    #Hirata MP2
    #generate Antisymmetric numerator
    W = np.zeros_like(VV)
    for i in range(rdim):
        for j in range(rdim):
            for k in range(rdim):
                for l in range(rdim):

                    W[i*rdim + j, k*rdim + l] = 2*VV[i*rdim + j, k*rdim +l] - VV[i*rdim + j, l*rdim + k]

    EMP2_Hirata = 0
    for i in occ_loc:
        for j in occ_loc:
            for a in virt_loc:
                for b in virt_loc:

                    #loop over k-points
                    for ki in range(K_L):
                        for kj in range(K_L):
                            for ka in range(K_L):

                                kb = (ki + kj - ka)%K_L
                                D = Eigs[ki][i] + Eigs[kj][j] - Eigs[ka][a] - Eigs[kb][b]

                                #get 2-electron integral numerator; indices --> integrals
                                ii = nao*ki + i
                                jj = nao*kj + j
                                aa = nao*ka + a
                                bb = nao*kb + b

                                numerator = (W[ii*rdim + jj, aa*rdim + bb].conjugate()*VV[ii*rdim + jj,
                                                aa*rdim + bb])

                                EMP2_Hirata += numerator/D

    EMP2_Hirata = EMP2_Hirata/K_L
    print EMP2_Hirata
    
                 
    
