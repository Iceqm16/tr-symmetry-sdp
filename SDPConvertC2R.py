'''
Take sdp file as input and return complexified version as output

'''

import numpy as np
import sys
from scipy.sparse import *
import itertools
import pickle


def readSDP(sdpfile):

    """
    Read SDP file in Mazziotti format
    """

    #use with open to handle errors
    with open(sdpfile,'r') as fid:
        line1 = fid.readline()
        #line1 = line1.split(' ')
        line1 = filter(None, line1.split(' '))
 
        try:
            nc = int(line1[0])
            nv = int(line1[1])
            nnz = int(line1[2])
            nb = int(line1[3].strip())
        except ValueError:
            line1 = line1[0].split('\t')
            nc = int(line1[0])
            nv = int(line1[1])
            nnz = int(line1[2])
            nb = int(line1[3].strip())
 
        #print "%i %i %i %i" % (nc,nv,nnz,nb)
        i = 0
        blocksize = []
        while i < nb:
            blocksize.append(int(fid.readline().split('\n')[0]))
            i+=1
        
        i = 0
        Amatrow = np.zeros(nnz)
        Amatcol = np.zeros(nnz)
        Amatarc = np.zeros(nnz)
        while i < nnz:
            line = fid.readline().strip().split(' ')
            line = [x for x in line if x!='']
            Amatrow[i] = int(line[0]) - 1 #added the -1 to avoid overflow
            Amatcol[i] = int(line[1]) - 1
            Amatarc[i] = float(line[2])
            i += 1

        i = 0
        bvec = np.zeros([nc])
        while i < nc:
            line = float(fid.readline().strip())
            bvec[i] = line
            i += 1

        i = 0
        cvec = np.zeros([nv])
        while i < nv:
            cvec[i] = float(fid.readline().strip())
            i += 1
            
    return blocksize,Amatrow,Amatcol,Amatarc,bvec,cvec, nc,nv,nnz,nb


def varmapper(start,startm,dim,vmap):
   
    """
    Map real matrix to complex matrix
    vmap[i] = ( (r1,r2), (c1,c2) , indicator_for_matrix_position(0==diagonal,
    -1==upper_triangle, 1==lower_triangle) )
    """

    cnt = start
    for x in range(dim):
        for y in range(dim):

            if x == y:
                vmap[cnt] = ( (x*2*dim + y + startm, x*2*dim+y + dim*(2*dim+1) + startm ), 
                (  x*2*dim + y + dim + startm, x*2*dim +y + (2*(dim**2)) + startm ), 0  )
            elif x > y:
                vmap[cnt] = ( (x*2*dim + y + startm, x*2*dim+y + dim*(2*dim+1) + startm ), 
                (  x*2*dim + y + dim + startm, x*2*dim +y + (2*(dim**2)) +
                    startm ), -1  )
            else:
                vmap[cnt] = ( (x*2*dim + y + startm, x*2*dim+y + dim*(2*dim+1) + startm ), 
                (  x*2*dim + y + dim + startm, x*2*dim +y + (2*(dim**2)) + startm ), 1  )
 

            cnt += 1

    return vmap, startm + (2*dim)**2, cnt

def varmapper2(start, startm, dim, vmap):

    """
    Map real matrix to complex matrix
    vmap[i] = ( (r1,r2), (c1,c2) , indicator_for_matrix_position(0==diagonal,
    -1==upper_triangle, 1==lower_triangle) )
    """

    #reshape is Fortran-order so [0,1,2,3] -> [[0,2],[1,3]]
    #this is important!
    new_complex_matrix = np.arange(startm, startm + (
        (2*dim)**2)).reshape((2*dim,2*dim),order='F') 
    R1 = new_complex_matrix[:dim,:dim]
    R2 = new_complex_matrix[dim:,dim:]
    I1 = new_complex_matrix[:dim,dim:]
    I2 = new_complex_matrix[dim:,:dim]

    cnt = start
    for x in range(dim):
        for y in range(dim):

            if x == y:
                vmap[cnt] = ( (R1[x,y], R2[x,y]), (I1[x,y], I2[x,y]), 0 )
            elif x > y:
                vmap[cnt] = ( (R1[x,y], R2[x,y]), (I1[x,y], I2[x,y]), 1)
            else:
                vmap[cnt] = ( (R1[x,y], R2[x,y]), (I1[x,y], I2[x,y]), -1 )
            cnt += 1
 
    return vmap, startm + (2*dim)**2, cnt 

def run1(sdpfile=None, K2Ainput=None, K2Sinput=None):

    if sdpfile == None:
        if ".sdp" not in sys.argv[1]:
            print "must give an sdp file as first input"
            sys.exit()
        else:
            sdpfile = sys.argv[1]
    
    print sdpfile
    #read SDP file
    blocksize, Amatrow, Amatcol, Amatarc,bvec, cvec, nc, nv,nnz,nb = readSDP(sdpfile)

    #read K2 if second input is provided.  Assign to cvec.  If not then make
    #cvec [[cvec, zeros],[zeros,cvec]]
    #if len(sys.argv) == 3:
    #    new_cvec = []
    #    for line in open(sys.argv[2], 'r'):
    #        new_cvec.append(float(line.strip()))
    #    cvec = new_cvec
    #else:
    #    #this functionality only works if Antisymmetric block is size == 1 and
    #    #symmetric block is size == 1
    #    K2A = cvec[:blocksize[0]**2].reshape((blocksize[0],blocksize[0]))
    #    K2S = cvec[blocksize[0]**2: blocksize[0]**2 + blocksize[1]**2].reshape((blocksize[1],blocksize[1]))

    #    K2A_complex = np.vstack( ( np.hstack( (K2A, np.zeros_like(K2A) ) ),
    #                    np.hstack( ( np.zeros_like(K2A) , K2A ) ) ) )
    #    K2S_complex = np.vstack( ( np.hstack( (K2S, np.zeros_like(K2S) ) ),
    #                    np.hstack( ( np.zeros_like(K2S) , K2S ) ) ) )

    #    new_cvec = []
    #    for x in range(K2A_complex.shape[0]):
    #        for y in range(K2A_complex.shape[1]):
    #            new_cvec.append(K2A_complex[x,y])
    #    for x in range(K2S_complex.shape[0]):
    #        for y in range(K2S_complex.shape[1]):
    #            new_cvec.append(K2S_complex[x,y])

    #    cvec = new_cvec 

    #create variable mapper
    vmap = {}
    start = startm = 0
    for i in blocksize:
        vmap, startm, start = varmapper2(start, startm, i, vmap)
    
            
    #map all constraints
    Amat = csr_matrix( (Amatarc,(Amatrow,Amatcol)), shape=(nc,nv))

    #generate coefficients for each constraint
    Anewarc = []
    Anewrow = []
    Anewcol = []
    bnew = []
    constraint_cnt = 0
    for i in range(nc):
        row = Amat.getrow(i).tocoo()
        tmp = np.zeros((row.nnz,7))
        cnt = 0
        #izip from itertools is fastest iterator build
        for j,v in itertools.izip(row.col,row.data): 
            tmp[cnt,0] = vmap[j][0][0]
            tmp[cnt,1] = vmap[j][0][1]
            tmp[cnt,2] = vmap[j][1][0]
            tmp[cnt,3] = vmap[j][1][1]
            tmp[cnt,4] = v
            #assign for coefficient for I1,I2
            if vmap[j][2] == 0 or vmap[j][2] == 1:
                tmp[cnt,5] = -v
                tmp[cnt,6] = v
            else:
                tmp[cnt,5] = v
                tmp[cnt,6] = -v
            #now check if map corresponds to diagonal elements.  if so then can
            #be removed for mapping G-matrix.  I believe without this there is
            #too much freedom in the phase of the G-matrix.
            cnt += 1

        Anewrow += [constraint_cnt]*2*row.nnz
        t = zip(list(tmp[:,0]) + list(tmp[:,1])  , list(tmp[:,4]) + list(tmp[:,4])  ) 
        t = sorted(t, key=lambda x: x[0])
        Anewcol += map(lambda x: x[0], t) 
        Anewarc += map(lambda x: x[1], t) 
        bnew.append(bvec[i]*2)
        constraint_cnt += 1


    for i in range(1,nc):
        row = Amat.getrow(i).tocoo()

        #now count the number of non-diagonal terms
        non_diag = 0
        for j,v in itertools.izip(row.col,row.data): 
            if vmap[j][2] != 0:
                non_diag += 1
 
        tmp = np.zeros((non_diag,7))
        cnt = 0
        #izip from itertools is fastest iterator build
        for j,v in itertools.izip(row.col,row.data): 
            if vmap[j][2] != 0:
                tmp[cnt,0] = vmap[j][0][0]
                tmp[cnt,1] = vmap[j][0][1]
                tmp[cnt,2] = vmap[j][1][0]
                tmp[cnt,3] = vmap[j][1][1]
                tmp[cnt,4] = v
                #assign for coefficient for I1
                if vmap[j][2] == 1:
                    tmp[cnt,5] = -v
                    tmp[cnt,6] = v
                else:
                    tmp[cnt,5] = v
                    tmp[cnt,6] = -v
            cnt += 1
      
        #now we have imaginary maps with correct sign on coefficients and no
        #diagonal elements
        Anewrow += [constraint_cnt]*2*non_diag
        t = zip(list(tmp[:,2]) + list(tmp[:,3]), list(tmp[:,5]) + list(tmp[:,6]) ) 
        t = sorted(t, key=lambda x: x[0])
        Anewcol += map(lambda x: x[0], t) 
        Anewarc += map(lambda x: x[1], t) 
        bnew.append(0)
        constraint_cnt += 1

        
    #hermtian and imaginary constraints. real == real, imag == imag, imag[diag] == 0
    #Put var numbs into matrix. Extract blocks.  Constraint reals and imag
    bigblocks = map(lambda x: 2*x, blocksize)
    count = 0 
    for i in bigblocks:
        tmp = np.zeros((i,i))
        for x in range(i):
            for y in range(i):

                tmp[y,x] = count
                count += 1

        #block out the tmp and add the correct constraints
        tmp_r1 = tmp[:i/2,:i/2]
        tmp_r2 = tmp[i/2 : , i/2 :]
        tmp_i1 = tmp[:i/2,i/2:]
        tmp_i2 = tmp[i/2 :, :i/2]

        ##constrain complex representation
        for xx in range(tmp_r1.shape[0]):
            for yy in range(xx, tmp_r1.shape[1]):
                if xx == yy:
                    #real constraint
                    Anewrow += [constraint_cnt]*2
                    t = zip([tmp_r1[xx,yy],  tmp_r2[xx,yy]]
                    , [1.0,-1.0])
                    t = sorted(t, key=lambda x: x[0])
                    Anewcol += map(lambda x: x[0], t)
                    Anewarc += map(lambda x: x[1], t)
                    bnew.append(0)
                    constraint_cnt += 1 
                    #imag constraint equal zero on diagonal
                    Anewrow += [constraint_cnt]*2
                    t = zip([tmp_i1[xx,yy], tmp_i2[xx,yy]], 
                            [1.0,1.0])
                    t = sorted(t, key=lambda x: x[0])
                    Anewcol += map(lambda x: x[0], t)
                    Anewarc += map(lambda x: x[1], t)
                    bnew.append(0)
                    constraint_cnt += 1 
                else:
                    #real constraint
                    Anewrow += [constraint_cnt]*4
                    t = zip([tmp_r1[xx,yy], tmp_r1[yy,xx], tmp_r2[xx,yy], tmp_r2[yy,xx]],
                            [1.0,1.0,-1.0,-1.0])
                    t = sorted(t, key=lambda x: x[0])
                    Anewcol += map(lambda x: x[0], t)
                    Anewarc += map(lambda x: x[1], t)
                    bnew.append(0)
                    constraint_cnt += 1 
                    #imag constraint
                    Anewrow += [constraint_cnt]*4
                    #both of these should work....
                    t = zip([tmp_i1[xx,yy], tmp_i2[yy,xx],tmp_i1[yy,xx], tmp_i2[xx,yy]], 
                            [1,-1,1,-1])
                    #t = zip([tmp_i1[xx,yy], tmp_i2[yy,xx],tmp_i1[yy,xx], tmp_i2[xx,yy]], 
                    #        [1,1,1,1])
                    t = sorted(t, key=lambda x: x[0])
                    Anewcol += map(lambda x: x[0], t)
                    Anewarc += map(lambda x: x[1], t)
                    bnew.append(0)
                    constraint_cnt += 1 


    #write new SDP
    with open("newNCR.sdp","w") as fid:

        new_nv = np.sum(map(lambda x: (2*x)**2, blocksize))
        new_nc = len(bnew)
        new_nnz = len(Anewrow)
        new_nb = nb
        fid.write("%i\t%i\t%i\t%i\n"%(new_nc, new_nv, new_nnz, new_nb))

        #writing new blocksizes
        for i in blocksize:
            fid.write("%i\n"%(2*i))

        #writing new non-zero constraints
        for i in range(new_nnz):
            fid.write("%i %i  %s\n"%(Anewrow[i]+1, Anewcol[i]+1, "{: .10f}".format(Anewarc[i])))

        #writing bvec
        fid.write(" %s\n"%("{: .10f}".format(bnew[0])) )
        for i in range(1,new_nc):
            fid.write("%i\n"%(bnew[i]))

        #writing cvec
        if K2Ainput == None:
            for i in range(new_nv):

                if i < len(cvec):
                    fid.write("%s\n"%("{: .10E}".format(cvec[i])))
                else:
                    fid.write("0\n")
        else:
            #unpackage and write into cvec
            print "unpacking K2"
            with open("K2A.p","wb") as pfid:
                pickle.dump( K2Ainput,  pfid )
            with open("K2S.p","wb") as pfid:
                pickle.dump( K2Sinput,  pfid )
 
            K2Antireal = []
            for mat in K2Ainput:
                               
                K2Antireal.append(
                    np.vstack( ( np.hstack( (mat.real, mat.imag) ), 
                                 np.hstack( (-1.*mat.imag,    mat.real) )  
                               ) 
                            )
                                )
                #K2Antireal.append(
                #    np.vstack( ( np.hstack( (mat.real, np.zeros_like(mat.imag) ) ), 
                #                 np.hstack( (np.zeros_like(mat.imag),    mat.real) )  
                #               ) 
                #            )
                #                )



            K2Symreal = []
            for mat in K2Sinput:


                K2Symreal.append( 
                np.vstack( ( np.hstack( (mat.real, mat.imag) ), 
                             np.hstack( (-1.*mat.imag,    mat.real) )  
                           ) 
                          )
                                )
                #K2Symreal.append( 
                #np.vstack( ( np.hstack( (mat.real, np.zeros_like(mat.imag) ) ), 
                #             np.hstack( ( np.zeros_like(mat.imag),    mat.real) )  
                #           ) 
                #          )
                #                )



            #write in fortran order
            cvec_big = np.zeros(new_nv)
            cnt = 0
            for x in range(len(K2Antireal)):
                for i in range(K2Antireal[x].shape[0]):
                    for j in range(K2Antireal[x].shape[1]):
                        #multiply by 3 because (s,m) = (1,1),(1,0) , (1,-1) all
                        #the same. 
                        cvec_big[cnt] = 3*K2Antireal[x][j,i]
                        cnt += 1
            for x in range(len(K2Symreal)):
                for i in range(K2Symreal[x].shape[0]):
                    for j in range(K2Symreal[x].shape[1]):
                        cvec_big[cnt] = K2Symreal[x][j,i]
                        cnt += 1
 
            for i in range(new_nv):

                fid.write("%s\n"%("{: .10E}".format(cvec_big[i])))



if __name__=="__main__":

    run1()
