Time-Reversal Symmetry Constraints
by Nicholas C. Rubin
-------

This repo contains various python scripts that take a CO-HF output and produce
an SDP with time-reversal equality constraints.  It also has code for MP2 on
k-space and some scripts to extract Mulliken populations and atomic densities.
The usual work flow is as follows:

1. Do a PBC-CO-HF calculations with PRINT 2
2. Apply  TR symmetry to integrals using TRSymmetry.py.  The integrals
generated from Namur cut off produce symmetry broken crystal orbitals
3. do makeSDP v67H to use Hubbard symm.  Should produce a SDP with blocking
corresponding to K_L (The CO-HF parameter)
4. Run complex2real.py on output of makeSDP to produce a TR-invariant SDP to run
with solver.  This program takes an Integral file, and a bas file.  It will
identify the (k,-k) pairs and explicitly constrain the density matrix.  It also
expresses a complex SDP (with complex integrals in the Intfile) as a real SDP.
