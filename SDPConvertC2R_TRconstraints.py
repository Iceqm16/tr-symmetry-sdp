'''
Take sdp file as input and return complexified version as output

'''

import numpy as np
import sys
from scipy.sparse import *
import itertools
import pickle
sys.path.append("/home/nick/pyncr/postprocess")
import RDMUtil_03112013 as RDMUtil
from glob import glob


def readSDP(sdpfile):

    """
    Read SDP file in Mazziotti format
    """

    #use with open to handle errors
    with open(sdpfile,'r') as fid:
        line1 = fid.readline()
        #line1 = line1.split(' ')
        line1 = filter(None, line1.split(' '))
 
        try:
            nc = int(line1[0])
            nv = int(line1[1])
            nnz = int(line1[2])
            nb = int(line1[3].strip())
        except ValueError:
            line1 = line1[0].split('\t')
            nc = int(line1[0])
            nv = int(line1[1])
            nnz = int(line1[2])
            nb = int(line1[3].strip())
 
        #print "%i %i %i %i" % (nc,nv,nnz,nb)
        i = 0
        blocksize = []
        while i < nb:
            blocksize.append(int(fid.readline().split('\n')[0]))
            i+=1
        
        i = 0
        Amatrow = np.zeros(nnz)
        Amatcol = np.zeros(nnz)
        Amatarc = np.zeros(nnz)
        while i < nnz:
            line = fid.readline().strip().split(' ')
            line = [x for x in line if x!='']
            Amatrow[i] = int(line[0]) - 1 #added the -1 to avoid overflow
            Amatcol[i] = int(line[1]) - 1
            Amatarc[i] = float(line[2])
            i += 1

        i = 0
        bvec = np.zeros([nc])
        while i < nc:
            line = float(fid.readline().strip())
            bvec[i] = line
            i += 1

        i = 0
        cvec = np.zeros([nv])
        while i < nv:
            cvec[i] = float(fid.readline().strip())
            i += 1
            
    return blocksize,Amatrow,Amatcol,Amatarc,bvec,cvec, nc,nv,nnz,nb


def varmapper(start,startm,dim,vmap):
   
    """
    Map real matrix to complex matrix
    vmap[i] = ( (r1,r2), (c1,c2) , indicator_for_matrix_position(0==diagonal,
    -1==upper_triangle, 1==lower_triangle) )
    """

    cnt = start
    for x in range(dim):
        for y in range(dim):

            if x == y:
                vmap[cnt] = ( (x*2*dim + y + startm, x*2*dim+y + dim*(2*dim+1) + startm ), 
                (  x*2*dim + y + dim + startm, x*2*dim +y + (2*(dim**2)) + startm ), 0  )
            elif x > y:
                vmap[cnt] = ( (x*2*dim + y + startm, x*2*dim+y + dim*(2*dim+1) + startm ), 
                (  x*2*dim + y + dim + startm, x*2*dim +y + (2*(dim**2)) +
                    startm ), -1  )
            else:
                vmap[cnt] = ( (x*2*dim + y + startm, x*2*dim+y + dim*(2*dim+1) + startm ), 
                (  x*2*dim + y + dim + startm, x*2*dim +y + (2*(dim**2)) + startm ), 1  )
 

            cnt += 1

    return vmap, startm + (2*dim)**2, cnt

def varmapper2(start, startm, dim, vmap):

    """
    Map real matrix to complex matrix
    vmap[i] = ( (r1,r2), (c1,c2) , indicator_for_matrix_position(0==diagonal,
    -1==upper_triangle, 1==lower_triangle) )
    """

    #reshape is Fortran-order so [0,1,2,3] -> [[0,2],[1,3]]
    #this is important!
    new_complex_matrix = np.arange(startm, startm + (
        (2*dim)**2)).reshape((2*dim,2*dim),order='F') 
    R1 = new_complex_matrix[:dim,:dim]
    R2 = new_complex_matrix[dim:,dim:]
    I1 = new_complex_matrix[:dim,dim:]
    I2 = new_complex_matrix[dim:,:dim]

    cnt = start
    for x in range(dim):
        for y in range(dim):

            if x == y:
                vmap[cnt] = ( (R1[x,y], R2[x,y]), (I1[x,y], I2[x,y]), 0 )
            elif x > y:
                #lower triangle is positive 1
                vmap[cnt] = ( (R1[x,y], R2[x,y]), (I1[x,y], I2[x,y]), 1)
            else:
                #upper triangle is negative
                vmap[cnt] = ( (R1[x,y], R2[x,y]), (I1[x,y], I2[x,y]), -1 )
            cnt += 1
 
    return vmap, startm + (2*dim)**2, cnt 

def FindD2pairs(bas, D2Abas, D2Sbas, K_L, nao):

        
    D2AbasKindex = {}
    bas_idx = 0
    for kbas in D2Abas:
        kval = (bas[kbas[0][0]-1][0] + bas[kbas[0][1]-1][0])%K_L
        D2AbasKindex[bas_idx] = kval
        bas_idx +=1
    D2AbasKindex_rev = dict(zip(D2AbasKindex.values(), D2AbasKindex.keys()))

    #loop over the basis dictionaries to find one that is smaller than pi and
    #greater than zero. after that related the elements of that dictionary to
    #the other
    print "Finding D2A pairs"
    D2Apairs = []
    cnt = 0
    for kbas_idx in range(len(D2Abas)):
        if D2AbasKindex[kbas_idx] < K_L/2 and D2AbasKindex[kbas_idx] > 0:
            #now loop over this and find the others
            tmp_dict = {}
            cnt = 0
            kval = D2AbasKindex[kbas_idx]
            #print "found k block ", kbas_idx, " with momentum ",  kval
            #print "found index of -k block ", D2AbasKindex_rev[(-kval)%K_L]
            nkb = D2AbasKindex_rev[(-kval)%K_L]
            for idx, gem in D2Abas[kbas_idx].iteritems():
                for idx2, gem2 in D2Abas[nkb].iteritems():

                    ki,ni = bas[gem[0]-1]
                    kj,nj = bas[gem[1]-1]
                    ka,na = bas[gem2[0]-1]
                    kb,nb = bas[gem2[1]-1]
                    if (-ki%K_L,ni) == (ka,na) and (-kj%K_L,nj) == (kb,nb):
                        #print idx,idx2,1, (ki,kj)
                        #block, index, geminal, block, index, geminal, phase
                        #D2Apairs[cnt] = (kbas_idx, idx, gem, nkb, idx2, gem2, 1)
                        tmp_dict[cnt] = (kbas_idx, idx, gem, nkb, idx2, gem2, 1)
                        print (kbas_idx, idx, gem, nkb, idx2, gem2, 1)
                        cnt += 1
                    elif (-ki%K_L,ni) == (kb,nb) and (-kj%K_L,nj) == (ka,na):
                        #print idx,idx2,-1, (ki,kj)
                        #block, index, geminal, block, index, geminal, phase
                        #D2Apairs[cnt] = (kbas_idx, idx, gem, nkb, idx2, gem2, -1)
                        tmp_dict[cnt] = (kbas_idx, idx, gem, nkb, idx2, gem2, -1)
                        print (kbas_idx, idx, gem, nkb, idx2, gem2, -1)
                        cnt += 1
            D2Apairs.append(tmp_dict)


    D2SbasKindex = {}
    bas_idx = 0
    for kbas in D2Sbas:
        kval = (bas[kbas[0][0]-1][0] + bas[kbas[0][1]-1][0])%K_L
        D2SbasKindex[bas_idx] = kval
        bas_idx += 1
    D2SbasKindex_rev = dict(zip(D2SbasKindex.values(), D2SbasKindex.keys()))
   
    print "Finding D2Spairs"
    D2Spairs = []
    cnt = 0
    for kbas_idx in range(len(D2Sbas)):
        if D2SbasKindex[kbas_idx] < K_L/2 and D2SbasKindex[kbas_idx] > 0:
            tmp_dict = {}
            cnt = 0

            kval = D2SbasKindex[kbas_idx]
            nkb = D2SbasKindex_rev[(-kval)%K_L]

            for idx, gem in D2Sbas[kbas_idx].iteritems():
                for idx2, gem2 in D2Sbas[nkb].iteritems():

                    ki, ni = bas[gem[0]-1]
                    kj, nj = bas[gem[1]-1]
                    ka,na = bas[gem2[0]-1]
                    kb,nb = bas[gem2[1]-1]
                    if (-ki%K_L,ni) == (ka,na) and (-kj%K_L,nj) == (kb,nb):
                        #print idx,idx2,1, (ki,kj)
                        #block, index, geminal, block, index, geminal, phase
                        #D2Spairs[cnt] = (kbas_idx, idx, gem, nkb, idx2, gem2, 1)
                        tmp_dict[cnt] = (kbas_idx, idx, gem, nkb, idx2, gem2, 1)
                        print (kbas_idx, idx, gem, nkb, idx2, gem2, 1)
                        cnt += 1
                    elif (-ki%K_L,ni) == (kb,nb) and (-kj%K_L,nj) == (ka,na):
                        #D2Spairs[cnt] = (kbas_idx, idx, gem, nkb, idx2, gem2, -1)
                        tmp_dict[cnt] = (kbas_idx, idx, gem, nkb, idx2, gem2, -1)
                        print (kbas_idx, idx, gem, nkb, idx2, gem2, -1)
                        cnt += 1
            D2Spairs.append(tmp_dict) 

    return D2Apairs, D2Spairs, D2AbasKindex, D2SbasKindex

def FindD1pairs(bas, D1bas,  K_L, nao):

    #D1pairs
    D1Kindex = {}
    bas_idx = 0
    for kbas in D1bas:
        kval = bas[kbas[0]-1][0]%K_L
        D1Kindex[bas_idx] = kval
        bas_idx += 1
    D1Kindex_rev = dict(zip(D1Kindex.values(), D1Kindex.keys()))

    print D1Kindex
    print "Finding D1pairs"
    D1pairs = []
    cnt = 0
    for kbas_idx in range(len(D1bas)):
        if D1Kindex[kbas_idx] < K_L/2 and D1Kindex[kbas_idx] > 0:

            tmp_dict = {}
            cnt = 0
            kval = D1Kindex[kbas_idx]
            nkb = D1Kindex_rev[(-kval)%K_L]

            for idx, orb in D1bas[kbas_idx].iteritems():
                for idx2, orb2 in D1bas[nkb].iteritems():

                    ki, ni = bas[orb-1]
                    ka,na = bas[orb2-1]
                    if (-ki%K_L,ni) == (ka,na):
                        #print idx,idx2,1, (ki,kj)
                        #block, index, geminal, block, index, geminal, phase
                        #D1pairs[cnt] = (kbas_idx, idx, orb, nkb, idx2, orb2, 1)
                        tmp_dict[cnt] = (kbas_idx, idx, orb, nkb, idx2, orb2, 1)
                        print (kbas_idx, idx, orb, nkb, idx2, orb2, 1)
                        cnt += 1
            D1pairs.append(tmp_dict)


    return D1pairs, D1Kindex


def FindG2pairs(bas, G2bas, K_L, nao):

        
    G2basKindex = {}
    bas_idx = 0
    for kbas in G2bas:
        #we have to have minus from creation annihilation algebra
        kval = (bas[kbas[0][0]-1][0] - bas[kbas[0][1]-1][0])%K_L
        G2basKindex[bas_idx] = kval
        bas_idx +=1
    G2basKindex_rev = dict(zip(G2basKindex.values(), G2basKindex.keys()))

    #loop over the basis dictionaries to find one that is smaller than pi and
    #greater than zero. after that related the elements of that dictionary to
    #the other
    print "Finding G2 pairs"
    G2pairs = []
    cnt = 0
    for kbas_idx in range(len(G2bas)):
        if G2basKindex[kbas_idx] < K_L/2 and G2basKindex[kbas_idx] > 0:

            tmp_dict = {}
            cnt = 0
            #now loop over this and find the others
            kval = G2basKindex[kbas_idx]
            #print "found k block ", kbas_idx, " with momentum ",  kval
            #print "found index of -k block ", D2AbasKindex_rev[(-kval)%K_L]
            nkb = G2basKindex_rev[(-kval)%K_L]
            for idx, gem in G2bas[kbas_idx].iteritems():
                for idx2, gem2 in G2bas[nkb].iteritems():

                    ki,ni = bas[gem[0]-1]
                    kj,nj = bas[gem[1]-1]
                    ka,na = bas[gem2[0]-1]
                    kb,nb = bas[gem2[1]-1]
                    if (-ki%K_L,ni) == (ka,na) and (-kj%K_L,nj) == (kb,nb):
                        #print idx,idx2,1, (ki,kj)
                        #block, index, geminal, block, index, geminal, phase
                        #G2pairs[cnt] = (kbas_idx, idx, gem, nkb, idx2, gem2, 1)
                        tmp_dict[cnt] = (kbas_idx, idx, gem, nkb, idx2, gem2, 1)
                        print (kbas_idx, idx, gem, nkb, idx2, gem2, 1)
                        cnt += 1
                    elif (-ki%K_L,ni) == (kb,nb) and (-kj%K_L,nj) == (ka,na):
                        #print idx,idx2,-1, (ki,kj)
                        #block, index, geminal, block, index, geminal, phase
                        #G2pairs[cnt] = (kbas_idx, idx, gem, nkb, idx2, gem2, -1)
                        tmp_dict[cnt] = (kbas_idx, idx, gem, nkb, idx2, gem2, -1)
                        print (kbas_idx, idx, gem, nkb, idx2, gem2, -1)
                        cnt += 1
            G2pairs.append(tmp_dict)

    return G2pairs, G2basKindex

def PrintBaswithK(D2Abas, D2Sbas, D1bas, G2bas, bas, K_L):

    with open("BasFileWithK.bas","w") as fid:
        fid.write("Basis Functions of D2a:\n")
        for bidx in range(len(D2Abas)):
            fid.write("block: %i\n"%len(D2Abas[bidx]))
            for idx, gem in D2Abas[bidx].iteritems():
                ki = bas[gem[0]-1][0]
                kj = bas[gem[1]-1][0]
                fid.write("%i\t%i\t%i\n"%(gem[0],gem[1],(ki+kj)%K_L ))
        fid.write("Basis Functions of D2s:\n")
        for bidx in range(len(D2Sbas)):
            fid.write("block: %i\n"%len(D2Sbas[bidx]))
            for idx, gem in D2Sbas[bidx].iteritems():
                ki = bas[gem[0]-1][0]
                kj = bas[gem[1]-1][0]
                fid.write("%i\t%i\t%i\n"%(gem[0],gem[1],(ki+kj)%K_L ))
        fid.write("Basis Functions of D1:\n")
        for bidx in range(len(D1bas)):
            fid.write("block: %i\n"%len(D1bas[bidx]))
            for idx, gem in D1bas[bidx].iteritems():
                ki = bas[gem-1][0]
                fid.write("%i\t%i\n"%(gem,ki%K_L ))
        fid.write("Basis Functions of G2:\n")
        for bidx in range(len(G2bas)):
            fid.write("block: %i\n"%len(G2bas[bidx]))
            for idx, gem in G2bas[bidx].iteritems():
                ki = bas[gem[0]-1][0]
                kj = bas[gem[1]-1][0]
                fid.write("%i\t%i\t%i\n"%(gem[0],gem[1],(ki-kj)%K_L ))
       



def run1(nao=2, K_L=4, sdpfile=None, K2Ainput=None, K2Sinput=None):

    if sdpfile == None:
        if ".sdp" not in sys.argv[1]:
            print "must give an sdp file as first input"
            sys.exit()
        else:
            sdpfile = sys.argv[1]
    
    print sdpfile
    #read SDP file
    blocksize, Amatrow, Amatcol, Amatarc,bvec, cvec, nc, nv,nnz,nb = readSDP(sdpfile)
    #read basfile
    basfile = "./ActSIntegralsDQG.bas"#glob("*.bas")[0]
    D2Abas, D2Sbas, D1bas, G2bas = RDMUtil.getBas(basfile)

    #create variable mapper
    vmap = {}
    start = startm = 0
    for i in blocksize:
        vmap, startm, start = varmapper2(start, startm, i, vmap)

    #get time-reversal pairs of states
    #single particle basis
    bas = {}
    for k in range(K_L):
        for m in range(nao):
            bas[k*nao + m] = (k, m)
    bas_rev = dict(zip(bas.values(), bas.keys()))

    #get basis pairs with phase
    PrintBaswithK(D2Abas, D2Sbas, D1bas, G2bas, bas, K_L)
    D2Apairs, D2Spairs, D2AbasKindex, D2SbasKindex = FindD2pairs(bas, D2Abas, D2Sbas, K_L, nao)
    D1pairs, D1basKindex = FindD1pairs(bas, D1bas,  K_L, nao)
    G2pairs, G2basKindex = FindG2pairs(bas, G2bas, K_L, nao)

    #get all variable numbers, starting from zero...remember david starts at 1
    blockvars = []
    cnt = 0
    for dim in blocksize:
        tmp = np.zeros((dim,dim),dtype=int)
        for x in range(dim):
            for y in range(dim):
                tmp[y,x] = cnt
                cnt += 1
        blockvars.append(tmp)

    #map all constraints
    Amat = csr_matrix( (Amatarc,(Amatrow,Amatcol)), shape=(nc,nv))

    #generate coefficients for each constraint
    Anewarc = []
    Anewrow = []
    Anewcol = []
    bnew = []
    constraint_cnt = 0
    for i in range(nc):
        row = Amat.getrow(i).tocoo()
        tmp = np.zeros((row.nnz,7))
        cnt = 0
        #izip from itertools is fastest iterator build
        for j,v in itertools.izip(row.col,row.data): 
            tmp[cnt,0] = vmap[j][0][0]
            tmp[cnt,1] = vmap[j][0][1]
            tmp[cnt,2] = vmap[j][1][0]
            tmp[cnt,3] = vmap[j][1][1]
            tmp[cnt,4] = v
            #assign for coefficient for I1,I2
            if vmap[j][2] == 0 or vmap[j][2] == 1:
                tmp[cnt,5] = -v
                tmp[cnt,6] = v
            else:
                tmp[cnt,5] = v
                tmp[cnt,6] = -v
            cnt += 1

        Anewrow += [constraint_cnt]*2*row.nnz
        t = zip(list(tmp[:,0]) + list(tmp[:,1])  , list(tmp[:,4]) + list(tmp[:,4])  ) 
        t = sorted(t, key=lambda x: x[0])
        Anewcol += map(lambda x: x[0], t) 
        Anewarc += map(lambda x: x[1], t) 
        bnew.append(bvec[i]*2)
        constraint_cnt += 1


    for i in range(1,nc):
        row = Amat.getrow(i).tocoo()

        #now count the number of non-diagonal terms
        non_diag = 0
        for j,v in itertools.izip(row.col,row.data): 
            if vmap[j][2] != 0:
                non_diag += 1
 
        tmp = np.zeros((non_diag,7))
        cnt = 0
        #izip from itertools is fastest iterator build
        for j,v in itertools.izip(row.col,row.data): 
            if vmap[j][2] != 0:
                tmp[cnt,0] = vmap[j][0][0]
                tmp[cnt,1] = vmap[j][0][1]
                tmp[cnt,2] = vmap[j][1][0]
                tmp[cnt,3] = vmap[j][1][1]
                tmp[cnt,4] = v
                #assign for coefficient for I1
                if vmap[j][2] == 1: #if it's on the lower triangle
                    tmp[cnt,5] = -v #upper triangle imaginary gets this coefficient
                    tmp[cnt,6] = v #lower triangle imaginary gets this coefficient
                else: #if it's on the upper triangle
                    tmp[cnt,5] = v #upper get this 
                    tmp[cnt,6] = -v #lower gets this
            cnt += 1
      
        #now we have imaginary maps with correct sign on coefficients and no
        #diagonal elements
        Anewrow += [constraint_cnt]*2*non_diag
        t = zip(list(tmp[:,2]) + list(tmp[:,3]), list(tmp[:,5]) + list(tmp[:,6]) ) 
        t = sorted(t, key=lambda x: x[0])
        Anewcol += map(lambda x: x[0], t) 
        Anewarc += map(lambda x: x[1], t) 
        bnew.append(0)
        constraint_cnt += 1

        
    #hermtian and imaginary constraints. real == real, imag == imag, imag[diag] == 0
    #Put var numbs into matrix. Extract blocks.  Constraint reals and imag
    bigblocks = map(lambda x: 2*x, blocksize)
    count = 0 
    for i in bigblocks:
        tmp = np.zeros((i,i))
        for x in range(i):
            for y in range(i):

                tmp[y,x] = count
                count += 1

        #block out the tmp and add the correct constraints
        tmp_r1 = tmp[:i/2,:i/2]
        tmp_r2 = tmp[i/2 : , i/2 :]
        tmp_i1 = tmp[:i/2,i/2:]
        tmp_i2 = tmp[i/2 :, :i/2]

        ##constrain complex representation
        for xx in range(tmp_r1.shape[0]):
            for yy in range(xx, tmp_r1.shape[1]):
                if xx == yy:
                    #real constraint
                    Anewrow += [constraint_cnt]*2
                    t = zip([tmp_r1[xx,yy],  tmp_r2[xx,yy]]
                    , [1.0,-1.0])
                    t = sorted(t, key=lambda x: x[0])
                    Anewcol += map(lambda x: x[0], t)
                    Anewarc += map(lambda x: x[1], t)
                    bnew.append(0)
                    constraint_cnt += 1 
                    #imag constraint equal zero on diagonal
                    Anewrow += [constraint_cnt]*2
                    t = zip([tmp_i1[xx,yy], tmp_i2[xx,yy]], 
                            [1.0,1.0])
                    t = sorted(t, key=lambda x: x[0])
                    Anewcol += map(lambda x: x[0], t)
                    Anewarc += map(lambda x: x[1], t)
                    bnew.append(0)
                    constraint_cnt += 1 
                else:
                    #real constraint
                    Anewrow += [constraint_cnt]*4
                    t = zip([tmp_r1[xx,yy], tmp_r1[yy,xx], tmp_r2[xx,yy], tmp_r2[yy,xx]],
                            [1.0,1.0,-1.0,-1.0])
                    t = sorted(t, key=lambda x: x[0])
                    Anewcol += map(lambda x: x[0], t)
                    Anewarc += map(lambda x: x[1], t)
                    bnew.append(0)
                    constraint_cnt += 1 


                    #imag constraint
                    #Anewrow += [constraint_cnt]*2
                    #both of these should work....or split into two constraints
                    #sets 
                    #t = zip([tmp_i1[xx,yy], tmp_i2[yy,xx],tmp_i1[yy,xx], tmp_i2[xx,yy]], 
                    #        [1,-1,1,-1])
                    Anewrow += [constraint_cnt]*2
                    t1 = zip([tmp_i1[xx,yy], tmp_i2[yy,xx]],[1,-1])
                    t1 = sorted(t1, key=lambda x: x[0])
                    Anewcol += map(lambda x: x[0], t1)
                    Anewarc += map(lambda x: x[1], t1)
                    bnew.append(0)
                    constraint_cnt += 1


                    Anewrow += [constraint_cnt]*2
                    t2 = zip([tmp_i1[yy,xx], tmp_i2[xx,yy]],[1,-1])
                    t2 = sorted(t2, key=lambda x: x[0])
                    Anewcol += map(lambda x: x[0], t2)
                    Anewarc += map(lambda x: x[1], t2)
                    bnew.append(0)
                    constraint_cnt += 1

                    #t = zip([tmp_i1[xx,yy], tmp_i2[yy,xx],tmp_i1[yy,xx], tmp_i2[xx,yy]], 
                    #        [1,1,1,1])
                    #t = sorted(t, key=lambda x: x[0])
                    #Anewcol += map(lambda x: x[0], t)
                    #Anewarc += map(lambda x: x[1], t)
                    #bnew.append(0)
                    #constraint_cnt += 1 



    ##loop over D2A matrices that have the correct k and find the correct pair set
    print constraint_cnt
    print D2AbasKindex
    print D2Apairs
    pair_idx = 0
    for kidx in range(len(D2AbasKindex)):
        if D2AbasKindex[kidx] > 0 and D2AbasKindex[kidx] < K_L/2:

            #now loop over matrix. for real it's pretty simple.  only do upper
            #half
            print "D1basKindex ", D2AbasKindex[kidx], kidx
            for xx in range(len(D2Abas[kidx])):
                for yy in range(len(D2Abas[kidx])):
                
                    kxblock, kxidx, kxgem, nkxblock, nkxidx, nkgem, xphase = D2Apairs[pair_idx][xx]
                    kyblock ,kyidx, kygem, nkyblock, nkyidx, nkygem, yphase = D2Apairs[pair_idx][yy]
                    #real constraints only need to do upper diagonal
                    if yy >= xx:
                        #get the variable numbers to set equal
                        if xx == yy:
                            vark = blockvars[(0*K_L)+kxblock][kxidx, kyidx]
                            varnk = blockvars[(0*K_L)+nkxblock][nkxidx, nkyidx]

                            ((vkr1, vkr2), (vki1, vki2), _) = vmap[vark]
                            ((vnkr1, vnkr2), (vnki1, vnki2), _) = vmap[varnk]

                            Anewrow += [constraint_cnt]*2
                            Anewcol += [vkr1,vnkr1]
                            Anewarc += [1,-1*xphase*yphase]
                            bnew.append(0)
                            constraint_cnt += 1
                            Anewrow += [constraint_cnt]*2
                            Anewcol += [vkr2,vnkr2]
                            Anewarc += [1,-1*xphase*yphase]
                            bnew.append(0)
                            constraint_cnt += 1
                        else:
                            varkxy = blockvars[0*K_L+kxblock][kxidx, kyidx]
                            varnkxy = blockvars[0*K_L+nkxblock][nkxidx, nkyidx]
                            varkyx = blockvars[0*K_L+kxblock][ kyidx, kxidx]
                            varnkyx = blockvars[0*K_L+nkxblock][ nkyidx, nkxidx ]

                            ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                            ((vnkxyr1, vnkxyr2), (vnkxyi1, vnkxyi2), _) = vmap[varnkxy]
                            ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]
                            ((vnkyxr1, vnkyxr2), (vnkyxi1, vnkyxi2), _) = vmap[varnkyx]
 
                            Anewrow += [constraint_cnt]*8
                            Anewcol += [vkxyr1,vkyxr1,vkxyr2,vkyxr2, vnkxyr1,vnkyxr1, vnkxyr2, vnkyxr2]
                            Anewarc += [1,1,1,1,
                                    -1*xphase*yphase,-1*xphase*yphase,-1*xphase*yphase,
                                    -1*xphase*yphase]
                            bnew.append(0)
                            constraint_cnt += 1
                            #Anewrow += [constraint_cnt]*4
                            #Anewcol += [vkxyr2,vkyxr2, vnkxyr2,vnkyxr2]
                            #Anewarc += [1,1,-1, -1]
                            #constraint_cnt += 1
                    #
                    ##imaginary we need I_{12} and -I_{21} information to make
                    ##symmetric constraint
                    if xx != yy:

                        varkxy = blockvars[0*K_L+kxblock][kxidx, kyidx]
                        varnkxy = blockvars[0*K_L+nkxblock][nkxidx, nkyidx]
                        varkyx = blockvars[0*K_L+kxblock][ kyidx, kxidx]
                        varnkyx = blockvars[0*K_L+nkxblock][ nkyidx, nkxidx ]

                        ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                        ((vnkxyr1, vnkxyr2), (vnkxyi1, vnkxyi2), _) = vmap[varnkxy]
                        ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]
                        ((vnkyxr1, vnkyxr2), (vnkyxi1, vnkyxi2), _) = vmap[varnkyx]

                        Anewrow += [constraint_cnt]*4
                        Anewcol += [vkxyi1, vkyxi2, vnkxyi1, vnkyxi2]
                        Anewarc += [1,1,1*xphase*yphase,1*xphase*yphase]
                        constraint_cnt += 1
                        bnew.append(0)

            pair_idx += 1
        elif D2AbasKindex[kidx] == 0 or D2AbasKindex[kidx] == K_L/2:
            #constrain imaginary blocks to be zero
            print "Constraining imaginary block to zero ", D2AbasKindex[kidx]
            for xx in range(len(D2Abas[kidx])):
                for yy in range(len(D2Abas[kidx])):

                    if xx != yy:

                        varkxy = blockvars[0*K_L+kidx][xx, yy]
                        varkyx = blockvars[0*K_L+kidx][ yy, xx]

                        ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                        ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]

                        Anewrow += [constraint_cnt]*2
                        Anewcol += [vkxyi1, vkyxi2]
                        Anewarc += [1,1]
                        constraint_cnt += 1
                        bnew.append(0)

    ##loop over D2S matrices that have the correct k and find the correct pair set
    print constraint_cnt
    print D2SbasKindex
    print D2Spairs
    pair_idx = 0
    for kidx in range(len(D2SbasKindex)):
        if D2SbasKindex[kidx] > 0 and D2SbasKindex[kidx] < K_L/2:

            #now loop over matrix. for real it's pretty simple.  only do upper
            #half
            print "D1basKindex ", D2SbasKindex[kidx], kidx
            for xx in range(len(D2Sbas[kidx])):
                for yy in range(len(D2Sbas[kidx])):
                
                    kxblock, kxidx, kxgem, nkxblock, nkxidx, nkgem, xphase = D2Spairs[pair_idx][xx]
                    kyblock ,kyidx, kygem, nkyblock, nkyidx, nkygem, yphase = D2Spairs[pair_idx][yy]
                    #real constraints only need to do upper diagonal
                    if yy >= xx:
                        #get the variable numbers to set equal
                        if xx == yy:
                            vark = blockvars[(1*K_L)+kxblock][kxidx, kyidx]
                            varnk = blockvars[(1*K_L)+nkxblock][nkxidx, nkyidx]

                            ((vkr1, vkr2), (vki1, vki2), _) = vmap[vark]
                            ((vnkr1, vnkr2), (vnki1, vnki2), _) = vmap[varnk]

                            Anewrow += [constraint_cnt]*2
                            Anewcol += [vkr1,vnkr1]
                            Anewarc += [1,-1]
                            bnew.append(0)
                            constraint_cnt += 1
                            Anewrow += [constraint_cnt]*2
                            Anewcol += [vkr2,vnkr2]
                            Anewarc += [1,-1]
                            bnew.append(0)
                            constraint_cnt += 1
                        else:
                            varkxy = blockvars[1*K_L+kxblock][kxidx, kyidx]
                            varnkxy = blockvars[1*K_L+nkxblock][nkxidx, nkyidx]
                            varkyx = blockvars[1*K_L+kxblock][ kyidx, kxidx]
                            varnkyx = blockvars[1*K_L+nkxblock][ nkyidx, nkxidx ]

                            ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                            ((vnkxyr1, vnkxyr2), (vnkxyi1, vnkxyi2), _) = vmap[varnkxy]
                            ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]
                            ((vnkyxr1, vnkyxr2), (vnkyxi1, vnkyxi2), _) = vmap[varnkyx]
 
                            Anewrow += [constraint_cnt]*8
                            Anewcol += [vkxyr1,vkyxr1,vkxyr2,vkyxr2, vnkxyr1,vnkyxr1, vnkxyr2, vnkyxr2]
                            Anewarc += [1,1,1,1,
                                    -1,-1,-1,-1]
                            bnew.append(0)
                            constraint_cnt += 1
                            #Anewrow += [constraint_cnt]*4
                            #Anewcol += [vkxyr2,vkyxr2, vnkxyr2,vnkyxr2]
                            #Anewarc += [1,1,-1, -1]
                            #constraint_cnt += 1
                    #
                    ##imaginary we need I_{12} and -I_{21} information to make
                    ##symmetric constraint
                    if xx != yy:

                        varkxy = blockvars[1*K_L+kxblock][kxidx, kyidx]
                        varnkxy = blockvars[1*K_L+nkxblock][nkxidx, nkyidx]
                        varkyx = blockvars[1*K_L+kxblock][ kyidx, kxidx]
                        varnkyx = blockvars[1*K_L+nkxblock][ nkyidx, nkxidx ]

                        ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                        ((vnkxyr1, vnkxyr2), (vnkxyi1, vnkxyi2), _) = vmap[varnkxy]
                        ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]
                        ((vnkyxr1, vnkyxr2), (vnkyxi1, vnkyxi2), _) = vmap[varnkyx]

                        Anewrow += [constraint_cnt]*4
                        Anewcol += [vkxyi1, vkyxi2, vnkxyi1, vnkyxi2]
                        Anewarc += [1,1,1,1]
                        constraint_cnt += 1
                        bnew.append(0)

            pair_idx += 1
        elif D2SbasKindex[kidx] == 0 or D2SbasKindex[kidx] == K_L/2:
            #constrain imaginary blocks to be zero
            print "Constraining imaginary block to zero ", D2SbasKindex[kidx]
            for xx in range(len(D2Sbas[kidx])):
                for yy in range(len(D2Sbas[kidx])):

                    if xx != yy:

                        varkxy = blockvars[1*K_L+kidx][xx, yy]
                        varkyx = blockvars[1*K_L+kidx][ yy, xx]

                        ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                        ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]

                        Anewrow += [constraint_cnt]*2
                        Anewcol += [vkxyi1, vkyxi2]
                        Anewarc += [1,1]
                        constraint_cnt += 1
                        bnew.append(0)



    ##loop over D1 matrices that have the correct k and find the correct pair set
    print constraint_cnt
    print D1basKindex
    print D1pairs
    pair_idx = 0
    for kidx in range(len(D1basKindex)):
        if D1basKindex[kidx] > 0 and D1basKindex[kidx] < K_L/2:

            #now loop over matrix. for real it's pretty simple.  only do upper
            #half
            print "D1basKindex ", D1basKindex[kidx], kidx
            for xx in range(len(D1bas[kidx])):
                for yy in range(len(D1bas[kidx])):
                
                    kxblock, kxidx, kxgem, nkxblock, nkxidx, nkgem, xphase = D1pairs[pair_idx][xx]
                    kyblock ,kyidx, kygem, nkyblock, nkyidx, nkygem, yphase = D1pairs[pair_idx][yy]
                    #real constraints only need to do upper diagonal
                    if yy >= xx:
                        #get the variable numbers to set equal
                        if xx == yy:
                            vark = blockvars[(2*K_L)+kxblock][kxidx, kyidx]
                            varnk = blockvars[(2*K_L)+nkxblock][nkxidx, nkyidx]

                            ((vkr1, vkr2), (vki1, vki2), _) = vmap[vark]
                            ((vnkr1, vnkr2), (vnki1, vnki2), _) = vmap[varnk]

                            Anewrow += [constraint_cnt]*2
                            Anewcol += [vkr1,vnkr1]
                            Anewarc += [1,-1]
                            bnew.append(0)
                            constraint_cnt += 1
                            Anewrow += [constraint_cnt]*2
                            Anewcol += [vkr2,vnkr2]
                            Anewarc += [1,-1]
                            bnew.append(0)
                            constraint_cnt += 1
                        else:
                            varkxy = blockvars[2*K_L+kxblock][kxidx, kyidx]
                            varnkxy = blockvars[2*K_L+nkxblock][nkxidx, nkyidx]
                            varkyx = blockvars[2*K_L+kxblock][ kyidx, kxidx]
                            varnkyx = blockvars[2*K_L+nkxblock][ nkyidx, nkxidx ]

                            ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                            ((vnkxyr1, vnkxyr2), (vnkxyi1, vnkxyi2), _) = vmap[varnkxy]
                            ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]
                            ((vnkyxr1, vnkyxr2), (vnkyxi1, vnkyxi2), _) = vmap[varnkyx]
 
                            Anewrow += [constraint_cnt]*8
                            Anewcol += [vkxyr1,vkyxr1,vkxyr2,vkyxr2, vnkxyr1,vnkyxr1, vnkxyr2, vnkyxr2]
                            Anewarc += [1,1,1,1,-1,-1,-1, -1]
                            bnew.append(0)
                            constraint_cnt += 1
                            #Anewrow += [constraint_cnt]*4
                            #Anewcol += [vkxyr2,vkyxr2, vnkxyr2,vnkyxr2]
                            #Anewarc += [1,1,-1, -1]
                            #constraint_cnt += 1
                    #
                    ##imaginary we need I_{12} and -I_{21} information to make
                    ##symmetric constraint
                    if xx != yy:

                        varkxy = blockvars[2*K_L+kxblock][kxidx, kyidx]
                        varnkxy = blockvars[2*K_L+nkxblock][nkxidx, nkyidx]
                        varkyx = blockvars[2*K_L+kxblock][ kyidx, kxidx]
                        varnkyx = blockvars[2*K_L+nkxblock][ nkyidx, nkxidx ]

                        ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                        ((vnkxyr1, vnkxyr2), (vnkxyi1, vnkxyi2), _) = vmap[varnkxy]
                        ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]
                        ((vnkyxr1, vnkyxr2), (vnkyxi1, vnkyxi2), _) = vmap[varnkyx]

                        Anewrow += [constraint_cnt]*4
                        Anewcol += [vkxyi1, vkyxi2, vnkxyi1, vnkyxi2]
                        Anewarc += [1,1,1,1]
                        constraint_cnt += 1
                        bnew.append(0)

            pair_idx += 1
        elif D1basKindex[kidx] == 0 or D1basKindex[kidx] == K_L/2:
            #constrain imaginary blocks to be zero
            print "Constraining imaginary block to zero ", D1basKindex[kidx]
            for xx in range(len(D1bas[kidx])):
                for yy in range(len(D1bas[kidx])):

                    if xx != yy:

                        varkxy = blockvars[2*K_L+kidx][xx, yy]
                        varkyx = blockvars[2*K_L+kidx][ yy, xx]

                        ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                        ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]

                        Anewrow += [constraint_cnt]*2
                        Anewcol += [vkxyi1, vkyxi2]
                        Anewarc += [1,1]
                        constraint_cnt += 1
                        bnew.append(0)

    ##loop over Q1 matrices that have the correct k and find the correct pair set
    print constraint_cnt
    print D1basKindex
    print D1pairs
    pair_idx = 0
    for kidx in range(len(D1basKindex)):
        if D1basKindex[kidx] > 0 and D1basKindex[kidx] < K_L/2:

            #now loop over matrix. for real it's pretty simple.  only do upper
            #half
            print "D1basKindex ", D1basKindex[kidx], kidx
            for xx in range(len(D1bas[kidx])):
                for yy in range(len(D1bas[kidx])):
                
                    kxblock, kxidx, kxgem, nkxblock, nkxidx, nkgem, xphase = D1pairs[pair_idx][xx]
                    kyblock ,kyidx, kygem, nkyblock, nkyidx, nkygem, yphase = D1pairs[pair_idx][yy]
                    #real constraints only need to do upper diagonal
                    if yy >= xx:
                        #get the variable numbers to set equal
                        if xx == yy:
                            vark = blockvars[(3*K_L)+kxblock][kxidx, kyidx]
                            varnk = blockvars[(3*K_L)+nkxblock][nkxidx, nkyidx]

                            ((vkr1, vkr2), (vki1, vki2), _) = vmap[vark]
                            ((vnkr1, vnkr2), (vnki1, vnki2), _) = vmap[varnk]

                            Anewrow += [constraint_cnt]*2
                            Anewcol += [vkr1,vnkr1]
                            Anewarc += [1,-1]
                            bnew.append(0)
                            constraint_cnt += 1
                            Anewrow += [constraint_cnt]*2
                            Anewcol += [vkr2,vnkr2]
                            Anewarc += [1,-1]
                            bnew.append(0)
                            constraint_cnt += 1
                        else:
                            varkxy = blockvars[3*K_L+kxblock][kxidx, kyidx]
                            varnkxy = blockvars[3*K_L+nkxblock][nkxidx, nkyidx]
                            varkyx = blockvars[3*K_L+kxblock][ kyidx, kxidx]
                            varnkyx = blockvars[3*K_L+nkxblock][ nkyidx, nkxidx ]

                            ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                            ((vnkxyr1, vnkxyr2), (vnkxyi1, vnkxyi2), _) = vmap[varnkxy]
                            ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]
                            ((vnkyxr1, vnkyxr2), (vnkyxi1, vnkyxi2), _) = vmap[varnkyx]
 
                            Anewrow += [constraint_cnt]*8
                            Anewcol += [vkxyr1,vkyxr1,vkxyr2,vkyxr2, vnkxyr1,vnkyxr1, vnkxyr2, vnkyxr2]
                            Anewarc += [1,1,1,1,-1,-1,-1, -1]
                            bnew.append(0)
                            constraint_cnt += 1
                            #Anewrow += [constraint_cnt]*4
                            #Anewcol += [vkxyr2,vkyxr2, vnkxyr2,vnkyxr2]
                            #Anewarc += [1,1,-1, -1]
                            #constraint_cnt += 1
                    #
                    ##imaginary we need I_{12} and -I_{21} information to make
                    ##symmetric constraint
                    if xx != yy:

                        varkxy = blockvars[3*K_L+kxblock][kxidx, kyidx]
                        varnkxy = blockvars[3*K_L+nkxblock][nkxidx, nkyidx]
                        varkyx = blockvars[3*K_L+kxblock][ kyidx, kxidx]
                        varnkyx = blockvars[3*K_L+nkxblock][ nkyidx, nkxidx ]

                        ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                        ((vnkxyr1, vnkxyr2), (vnkxyi1, vnkxyi2), _) = vmap[varnkxy]
                        ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]
                        ((vnkyxr1, vnkyxr2), (vnkyxi1, vnkyxi2), _) = vmap[varnkyx]

                        Anewrow += [constraint_cnt]*4
                        Anewcol += [vkxyi1, vkyxi2, vnkxyi1, vnkyxi2]
                        Anewarc += [1,1,1,1]
                        constraint_cnt += 1
                        bnew.append(0)

            pair_idx += 1
        elif D1basKindex[kidx] == 0 or D1basKindex[kidx] == K_L/2:
            #constrain imaginary blocks to be zero
            print "Constraining imaginary block to zero ", D1basKindex[kidx]
            for xx in range(len(D1bas[kidx])):
                for yy in range(len(D1bas[kidx])):

                    if xx != yy:

                        varkxy = blockvars[3*K_L+kidx][xx, yy]
                        varkyx = blockvars[3*K_L+kidx][ yy, xx]

                        ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                        ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]

                        Anewrow += [constraint_cnt]*2
                        Anewcol += [vkxyi1, vkyxi2]
                        Anewarc += [1,1]
                        constraint_cnt += 1
                        bnew.append(0)


    ###loop over q2A matrices that have the correct k and find the correct pair set
    print constraint_cnt
    print D2AbasKindex
    print D2Apairs
    pair_idx = 0
    for kidx in range(len(D2AbasKindex)):
        if D2AbasKindex[kidx] > 0 and D2AbasKindex[kidx] < K_L/2:

            #now loop over matrix. for real it's pretty simple.  only do upper
            #half
            print "D1basKindex ", D2AbasKindex[kidx], kidx
            for xx in range(len(D2Abas[kidx])):
                for yy in range(len(D2Abas[kidx])):
                
                    kxblock, kxidx, kxgem, nkxblock, nkxidx, nkgem, xphase = D2Apairs[pair_idx][xx]
                    kyblock ,kyidx, kygem, nkyblock, nkyidx, nkygem, yphase = D2Apairs[pair_idx][yy]
                    #real constraints only need to do upper diagonal
                    if yy >= xx:
                        #get the variable numbers to set equal
                        if xx == yy:
                            vark = blockvars[(4*K_L)+kxblock][kxidx, kyidx]
                            varnk = blockvars[(4*K_L)+nkxblock][nkxidx, nkyidx]

                            ((vkr1, vkr2), (vki1, vki2), _) = vmap[vark]
                            ((vnkr1, vnkr2), (vnki1, vnki2), _) = vmap[varnk]

                            Anewrow += [constraint_cnt]*2
                            Anewcol += [vkr1,vnkr1]
                            Anewarc += [1,-1*xphase*yphase]
                            bnew.append(0)
                            constraint_cnt += 1
                            Anewrow += [constraint_cnt]*2
                            Anewcol += [vkr2,vnkr2]
                            Anewarc += [1,-1*xphase*yphase]
                            bnew.append(0)
                            constraint_cnt += 1
                        else:
                            varkxy = blockvars[4*K_L+kxblock][kxidx, kyidx]
                            varnkxy = blockvars[4*K_L+nkxblock][nkxidx, nkyidx]
                            varkyx = blockvars[4*K_L+kxblock][ kyidx, kxidx]
                            varnkyx = blockvars[4*K_L+nkxblock][ nkyidx, nkxidx ]

                            ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                            ((vnkxyr1, vnkxyr2), (vnkxyi1, vnkxyi2), _) = vmap[varnkxy]
                            ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]
                            ((vnkyxr1, vnkyxr2), (vnkyxi1, vnkyxi2), _) = vmap[varnkyx]
 
                            Anewrow += [constraint_cnt]*8
                            Anewcol += [vkxyr1,vkyxr1,vkxyr2,vkyxr2, vnkxyr1,vnkyxr1, vnkxyr2, vnkyxr2]
                            Anewarc += [1,1,1,1,
                                    -1*xphase*yphase,-1*xphase*yphase,-1*xphase*yphase,
                                    -1*xphase*yphase]
                            bnew.append(0)
                            constraint_cnt += 1
                            #Anewrow += [constraint_cnt]*4
                            #Anewcol += [vkxyr2,vkyxr2, vnkxyr2,vnkyxr2]
                            #Anewarc += [1,1,-1, -1]
                            #constraint_cnt += 1
                    #
                    ##imaginary we need I_{12} and -I_{21} information to make
                    ##symmetric constraint
                    if xx != yy:

                        varkxy = blockvars[4*K_L+kxblock][kxidx, kyidx]
                        varnkxy = blockvars[4*K_L+nkxblock][nkxidx, nkyidx]
                        varkyx = blockvars[4*K_L+kxblock][ kyidx, kxidx]
                        varnkyx = blockvars[4*K_L+nkxblock][ nkyidx, nkxidx ]

                        ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                        ((vnkxyr1, vnkxyr2), (vnkxyi1, vnkxyi2), _) = vmap[varnkxy]
                        ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]
                        ((vnkyxr1, vnkyxr2), (vnkyxi1, vnkyxi2), _) = vmap[varnkyx]

                        Anewrow += [constraint_cnt]*4
                        Anewcol += [vkxyi1, vkyxi2, vnkxyi1, vnkyxi2]
                        Anewarc += [1,1,1*xphase*yphase,1*xphase*yphase]
                        constraint_cnt += 1
                        bnew.append(0)

            pair_idx += 1
        elif D2AbasKindex[kidx] == 0 or D2AbasKindex[kidx] == K_L/2:
            #constrain imaginary blocks to be zero
            print "Constraining imaginary block to zero ", D2AbasKindex[kidx]
            for xx in range(len(D2Abas[kidx])):
                for yy in range(len(D2Abas[kidx])):

                    if xx != yy:

                        varkxy = blockvars[4*K_L+kidx][xx, yy]
                        varkyx = blockvars[4*K_L+kidx][ yy, xx]

                        ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                        ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]

                        Anewrow += [constraint_cnt]*2
                        Anewcol += [vkxyi1, vkyxi2]
                        Anewarc += [1,1]
                        constraint_cnt += 1
                        bnew.append(0)

    ##loop over D2S matrices that have the correct k and find the correct pair set
    print constraint_cnt
    print D2SbasKindex
    print D2Spairs
    pair_idx = 0
    for kidx in range(len(D2SbasKindex)):
        if D2SbasKindex[kidx] > 0 and D2SbasKindex[kidx] < K_L/2:

            #now loop over matrix. for real it's pretty simple.  only do upper
            #half
            print "D1basKindex ", D2SbasKindex[kidx], kidx
            for xx in range(len(D2Sbas[kidx])):
                for yy in range(len(D2Sbas[kidx])):
                
                    kxblock, kxidx, kxgem, nkxblock, nkxidx, nkgem, xphase = D2Spairs[pair_idx][xx]
                    kyblock ,kyidx, kygem, nkyblock, nkyidx, nkygem, yphase = D2Spairs[pair_idx][yy]
                    #real constraints only need to do upper diagonal
                    if yy >= xx:
                        #get the variable numbers to set equal
                        if xx == yy:
                            vark = blockvars[(5*K_L)+kxblock][kxidx, kyidx]
                            varnk = blockvars[(5*K_L)+nkxblock][nkxidx, nkyidx]

                            ((vkr1, vkr2), (vki1, vki2), _) = vmap[vark]
                            ((vnkr1, vnkr2), (vnki1, vnki2), _) = vmap[varnk]

                            Anewrow += [constraint_cnt]*2
                            Anewcol += [vkr1,vnkr1]
                            Anewarc += [1,-1]
                            bnew.append(0)
                            constraint_cnt += 1
                            Anewrow += [constraint_cnt]*2
                            Anewcol += [vkr2,vnkr2]
                            Anewarc += [1,-1]
                            bnew.append(0)
                            constraint_cnt += 1
                        else:
                            varkxy = blockvars[5*K_L+kxblock][kxidx, kyidx]
                            varnkxy = blockvars[5*K_L+nkxblock][nkxidx, nkyidx]
                            varkyx = blockvars[5*K_L+kxblock][ kyidx, kxidx]
                            varnkyx = blockvars[5*K_L+nkxblock][ nkyidx, nkxidx ]

                            ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                            ((vnkxyr1, vnkxyr2), (vnkxyi1, vnkxyi2), _) = vmap[varnkxy]
                            ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]
                            ((vnkyxr1, vnkyxr2), (vnkyxi1, vnkyxi2), _) = vmap[varnkyx]
 
                            Anewrow += [constraint_cnt]*8
                            Anewcol += [vkxyr1,vkyxr1,vkxyr2,vkyxr2, vnkxyr1,vnkyxr1, vnkxyr2, vnkyxr2]
                            Anewarc += [1,1,1,1,
                                    -1,-1,-1,
                                    -1]
                            bnew.append(0)
                            constraint_cnt += 1
                            #Anewrow += [constraint_cnt]*4
                            #Anewcol += [vkxyr2,vkyxr2, vnkxyr2,vnkyxr2]
                            #Anewarc += [1,1,-1, -1]
                            #constraint_cnt += 1
                    #
                    ##imaginary we need I_{12} and -I_{21} information to make
                    ##symmetric constraint
                    if xx != yy:

                        varkxy = blockvars[5*K_L+kxblock][kxidx, kyidx]
                        varnkxy = blockvars[5*K_L+nkxblock][nkxidx, nkyidx]
                        varkyx = blockvars[5*K_L+kxblock][ kyidx, kxidx]
                        varnkyx = blockvars[5*K_L+nkxblock][ nkyidx, nkxidx ]

                        ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                        ((vnkxyr1, vnkxyr2), (vnkxyi1, vnkxyi2), _) = vmap[varnkxy]
                        ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]
                        ((vnkyxr1, vnkyxr2), (vnkyxi1, vnkyxi2), _) = vmap[varnkyx]

                        Anewrow += [constraint_cnt]*4
                        Anewcol += [vkxyi1, vkyxi2, vnkxyi1, vnkyxi2]
                        Anewarc += [1,1,1,1]
                        constraint_cnt += 1
                        bnew.append(0)

            pair_idx += 1
        elif D2SbasKindex[kidx] == 0 or D2SbasKindex[kidx] == K_L/2:
            #constrain imaginary blocks to be zero
            print "Constraining imaginary block to zero ", D2SbasKindex[kidx]
            for xx in range(len(D2Sbas[kidx])):
                for yy in range(len(D2Sbas[kidx])):

                    if xx != yy:

                        varkxy = blockvars[5*K_L+kidx][xx, yy]
                        varkyx = blockvars[5*K_L+kidx][ yy, xx]

                        ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                        ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]

                        Anewrow += [constraint_cnt]*2
                        Anewcol += [vkxyi1, vkyxi2]
                        Anewarc += [1,1]
                        constraint_cnt += 1
                        bnew.append(0)

    ###loop over G2A matrices that have the correct k and find the correct pair set
    print constraint_cnt
    print G2basKindex
    print G2pairs
    pair_idx = 0
    for kidx in range(len(G2basKindex)):
        if G2basKindex[kidx] > 0 and G2basKindex[kidx] < K_L/2:

            #now loop over matrix. for real it's pretty simple.  only do upper
            #half
            print "D1basKindex ", G2basKindex[kidx], kidx
            for xx in range(len(G2bas[kidx])):
                for yy in range(len(G2bas[kidx])):
                
                    kxblock, kxidx, kxgem, nkxblock, nkxidx, nkgem, xphase = G2pairs[pair_idx][xx]
                    kyblock ,kyidx, kygem, nkyblock, nkyidx, nkygem, yphase = G2pairs[pair_idx][yy]
                    #real constraints only need to do upper diagonal
                    if yy >= xx:
                        #get the variable numbers to set equal
                        if xx == yy:
                            vark = blockvars[(6*K_L)+kxblock][kxidx, kyidx]
                            varnk = blockvars[(6*K_L)+nkxblock][nkxidx, nkyidx]

                            ((vkr1, vkr2), (vki1, vki2), _) = vmap[vark]
                            ((vnkr1, vnkr2), (vnki1, vnki2), _) = vmap[varnk]

                            Anewrow += [constraint_cnt]*2
                            Anewcol += [vkr1,vnkr1]
                            Anewarc += [1,-1*xphase*yphase]
                            bnew.append(0)
                            constraint_cnt += 1
                            Anewrow += [constraint_cnt]*2
                            Anewcol += [vkr2,vnkr2]
                            Anewarc += [1,-1*xphase*yphase]
                            bnew.append(0)
                            constraint_cnt += 1
                        else:
                            varkxy = blockvars[6*K_L+kxblock][kxidx, kyidx]
                            varnkxy = blockvars[6*K_L+nkxblock][nkxidx, nkyidx]
                            varkyx = blockvars[6*K_L+kxblock][ kyidx, kxidx]
                            varnkyx = blockvars[6*K_L+nkxblock][ nkyidx, nkxidx ]

                            ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                            ((vnkxyr1, vnkxyr2), (vnkxyi1, vnkxyi2), _) = vmap[varnkxy]
                            ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]
                            ((vnkyxr1, vnkyxr2), (vnkyxi1, vnkyxi2), _) = vmap[varnkyx]
 
                            Anewrow += [constraint_cnt]*8
                            Anewcol += [vkxyr1,vkyxr1,vkxyr2,vkyxr2, vnkxyr1,vnkyxr1, vnkxyr2, vnkyxr2]
                            Anewarc += [1,1,1,1,
                                    -1*xphase*yphase,-1*xphase*yphase,-1*xphase*yphase,
                                    -1*xphase*yphase]
                            bnew.append(0)
                            constraint_cnt += 1
                            #Anewrow += [constraint_cnt]*4
                            #Anewcol += [vkxyr2,vkyxr2, vnkxyr2,vnkyxr2]
                            #Anewarc += [1,1,-1, -1]
                            #constraint_cnt += 1
                    #
                    ##imaginary we need I_{12} and -I_{21} information to make
                    ##symmetric constraint
                    if xx != yy:

                        varkxy = blockvars[6*K_L+kxblock][kxidx, kyidx]
                        varnkxy = blockvars[6*K_L+nkxblock][nkxidx, nkyidx]
                        varkyx = blockvars[6*K_L+kxblock][ kyidx, kxidx]
                        varnkyx = blockvars[6*K_L+nkxblock][ nkyidx, nkxidx ]

                        ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                        ((vnkxyr1, vnkxyr2), (vnkxyi1, vnkxyi2), _) = vmap[varnkxy]
                        ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]
                        ((vnkyxr1, vnkyxr2), (vnkyxi1, vnkyxi2), _) = vmap[varnkyx]

                        Anewrow += [constraint_cnt]*4
                        Anewcol += [vkxyi1, vkyxi2, vnkxyi1, vnkyxi2]
                        Anewarc += [1,1,1*xphase*yphase,1*xphase*yphase]
                        constraint_cnt += 1
                        bnew.append(0)

            pair_idx += 1
        elif G2basKindex[kidx] == 0 or G2basKindex[kidx] == K_L/2:
            #constrain imaginary blocks to be zero
            print "Constraining imaginary block to zero ", G2basKindex[kidx]
            for xx in range(len(G2bas[kidx])):
                for yy in range(len(G2bas[kidx])):

                    if xx != yy:

                        varkxy = blockvars[6*K_L+kidx][xx, yy]
                        varkyx = blockvars[6*K_L+kidx][ yy, xx]

                        ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                        ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]

                        Anewrow += [constraint_cnt]*2
                        Anewcol += [vkxyi1, vkyxi2]
                        Anewarc += [1,1]
                        constraint_cnt += 1
                        bnew.append(0)

    ##loop over G2S matrices that have the correct k and find the correct pair set
    print constraint_cnt
    print G2basKindex
    print G2pairs
    pair_idx = 0
    for kidx in range(len(G2basKindex)):
        if G2basKindex[kidx] > 0 and G2basKindex[kidx] < K_L/2:

            #now loop over matrix. for real it's pretty simple.  only do upper
            #half
            print "D1basKindex ", G2basKindex[kidx], kidx
            for xx in range(len(G2bas[kidx])):
                for yy in range(len(G2bas[kidx])):
                
                    kxblock, kxidx, kxgem, nkxblock, nkxidx, nkgem, xphase = G2pairs[pair_idx][xx]
                    kyblock ,kyidx, kygem, nkyblock, nkyidx, nkygem, yphase = G2pairs[pair_idx][yy]
                    #real constraints only need to do upper diagonal
                    if yy >= xx:
                        #get the variable numbers to set equal
                        if xx == yy:
                            vark = blockvars[(7*K_L)+kxblock][kxidx, kyidx]
                            varnk = blockvars[(7*K_L)+nkxblock][nkxidx, nkyidx]

                            ((vkr1, vkr2), (vki1, vki2), _) = vmap[vark]
                            ((vnkr1, vnkr2), (vnki1, vnki2), _) = vmap[varnk]

                            Anewrow += [constraint_cnt]*2
                            Anewcol += [vkr1,vnkr1]
                            Anewarc += [1,-1]
                            bnew.append(0)
                            constraint_cnt += 1
                            Anewrow += [constraint_cnt]*2
                            Anewcol += [vkr2,vnkr2]
                            Anewarc += [1,-1]
                            bnew.append(0)
                            constraint_cnt += 1
                        else:
                            varkxy = blockvars[7*K_L+kxblock][kxidx, kyidx]
                            varnkxy = blockvars[7*K_L+nkxblock][nkxidx, nkyidx]
                            varkyx = blockvars[7*K_L+kxblock][ kyidx, kxidx]
                            varnkyx = blockvars[7*K_L+nkxblock][ nkyidx, nkxidx ]

                            ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                            ((vnkxyr1, vnkxyr2), (vnkxyi1, vnkxyi2), _) = vmap[varnkxy]
                            ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]
                            ((vnkyxr1, vnkyxr2), (vnkyxi1, vnkyxi2), _) = vmap[varnkyx]
 
                            Anewrow += [constraint_cnt]*8
                            Anewcol += [vkxyr1,vkyxr1,vkxyr2,vkyxr2, vnkxyr1,vnkyxr1, vnkxyr2, vnkyxr2]
                            Anewarc += [1,1,1,1,
                                    -1,-1,-1,
                                    -1]
                            bnew.append(0)
                            constraint_cnt += 1
                            #Anewrow += [constraint_cnt]*4
                            #Anewcol += [vkxyr2,vkyxr2, vnkxyr2,vnkyxr2]
                            #Anewarc += [1,1,-1, -1]
                            #constraint_cnt += 1
                    #
                    ##imaginary we need I_{12} and -I_{21} information to make
                    ##symmetric constraint
                    if xx != yy:

                        varkxy = blockvars[7*K_L+kxblock][kxidx, kyidx]
                        varnkxy = blockvars[7*K_L+nkxblock][nkxidx, nkyidx]
                        varkyx = blockvars[7*K_L+kxblock][ kyidx, kxidx]
                        varnkyx = blockvars[7*K_L+nkxblock][ nkyidx, nkxidx ]

                        ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                        ((vnkxyr1, vnkxyr2), (vnkxyi1, vnkxyi2), _) = vmap[varnkxy]
                        ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]
                        ((vnkyxr1, vnkyxr2), (vnkyxi1, vnkyxi2), _) = vmap[varnkyx]

                        Anewrow += [constraint_cnt]*4
                        Anewcol += [vkxyi1, vkyxi2, vnkxyi1, vnkyxi2]
                        Anewarc += [1,1,1,1]
                        constraint_cnt += 1
                        bnew.append(0)

            pair_idx += 1
        elif G2basKindex[kidx] == 0 or G2basKindex[kidx] == K_L/2:
            #constrain imaginary blocks to be zero
            print "Constraining imaginary block to zero ", G2basKindex[kidx]
            for xx in range(len(G2bas[kidx])):
                for yy in range(len(G2bas[kidx])):

                    if xx != yy:

                        varkxy = blockvars[7*K_L+kidx][xx, yy]
                        varkyx = blockvars[7*K_L+kidx][ yy, xx]

                        ((vkxyr1, vkxyr2), (vkxyi1, vkxyi2), _) = vmap[varkxy]
                        ((vkyxr1, vkyxr2), (vkyxi1, vkyxi2), _) = vmap[varkyx]

                        Anewrow += [constraint_cnt]*2
                        Anewcol += [vkxyi1, vkyxi2]
                        Anewarc += [1,1]
                        constraint_cnt += 1
                        bnew.append(0)





    
    #write new SDP
    with open("newNCR.sdp","w") as fid:

        new_nv = np.sum(map(lambda x: (2*x)**2, blocksize))
        new_nc = len(bnew)
        new_nnz = len(Anewrow)
        new_nb = nb
        fid.write("%i\t%i\t%i\t%i\n"%(new_nc, new_nv, new_nnz, new_nb))

        #writing new blocksizes
        for i in blocksize:
            fid.write("%i\n"%(2*i))

        #writing new non-zero constraints
        for i in range(new_nnz):
            fid.write("%i %i  %s\n"%(Anewrow[i]+1, Anewcol[i]+1, "{: .10f}".format(Anewarc[i])))

        #writing bvec
        fid.write(" %s\n"%("{: .10f}".format(bnew[0])) )
        for i in range(1,new_nc):
            fid.write("%i\n"%(bnew[i]))

        #writing cvec
        if K2Ainput == None:
            for i in range(new_nv):

                if i < len(cvec):
                    fid.write("%s\n"%("{: .10E}".format(cvec[i])))
                else:
                    fid.write("0\n")
        else:
            #unpackage and write into cvec
            print "unpacking K2"
            with open("K2A.p","wb") as pfid:
                pickle.dump( K2Ainput,  pfid )
            with open("K2S.p","wb") as pfid:
                pickle.dump( K2Sinput,  pfid )
 
            K2Antireal = []
            for mat in K2Ainput:
                               
                K2Antireal.append(
                    np.vstack( ( np.hstack( (mat.real, mat.imag) ), 
                                 np.hstack( (-1.*mat.imag,    mat.real) )  
                               ) 
                            )
                                )
                #K2Antireal.append(
                #    np.vstack( ( np.hstack( (mat.real, np.zeros_like(mat.imag) ) ), 
                #                 np.hstack( (np.zeros_like(mat.imag),    mat.real) )  
                #               ) 
                #            )
                #                )



            K2Symreal = []
            for mat in K2Sinput:


                K2Symreal.append( 
                np.vstack( ( np.hstack( (mat.real, mat.imag) ), 
                             np.hstack( (-1.*mat.imag,    mat.real) )  
                           ) 
                          )
                                )
                #K2Symreal.append( 
                #np.vstack( ( np.hstack( (mat.real, np.zeros_like(mat.imag) ) ), 
                #             np.hstack( ( np.zeros_like(mat.imag),    mat.real) )  
                #           ) 
                #          )
                #                )



            #write in fortran order
            cvec_big = np.zeros(new_nv)
            cnt = 0
            for x in range(len(K2Antireal)):
                for i in range(K2Antireal[x].shape[0]):
                    for j in range(K2Antireal[x].shape[1]):
                        #multiply by 3 because (s,m) = (1,1),(1,0) , (1,-1) all
                        #the same. 
                        cvec_big[cnt] = 3*K2Antireal[x][j,i]
                        cnt += 1
            for x in range(len(K2Symreal)):
                for i in range(K2Symreal[x].shape[0]):
                    for j in range(K2Symreal[x].shape[1]):
                        cvec_big[cnt] = K2Symreal[x][j,i]
                        cnt += 1
 
            for i in range(new_nv):

                fid.write("%s\n"%("{: .10E}".format(cvec_big[i])))



if __name__=="__main__":

    run1(nao=2,K_L=4)
