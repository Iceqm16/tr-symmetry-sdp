'''
take the output from an active space calculation and return a set of position
space density matrices

'''

import os
import numpy as np
import sys
sys.path.append('/home/nick/pyncr/postprocess/')
import RDMUtil_03112013 as RDMUtil
from glob import glob
import re
from scipy.linalg import block_diag
from itertools import product
from GetCoeffsAndOverlaps import *
import pickle




if __name__=="__main__":

    angstrom_to_bohr = 1. / 0.52917721092
    
    os.chdir(sys.argv[1])
    #get files
    sdpfile  = glob("*.sdp")[0]
    outfile = glob("*.out")[0]
    basfile = glob('*.bas')[0]
    ofile = min(glob("*.o"),key=len)
    with open(ofile,"r") as fid:
        text = fid.read()
        p = re.search("(basis rank =)\s+\w+", text)
        basis_rank = int(p.group().split('=')[1])
        p = re.search("(K_L =)\s+\w+", text)
        K_L = int(p.group().split('=')[1])
        nao = basis_rank
        p = re.search("(Nuclear repulsion energy =)\s+[0-9.-]+", text)
        enuc = float(p.group().split('=')[1])
        p = re.search("(xD block AO Norm =)\s+[0-9.-]+", text)
        num_el = int(float(p.group().split('=')[1]))
        p = re.search("(Total Energy)\s+[0-9.-]+", text)
        HF_kspace_energy = float(p.group().split(' ')[-1])
        p = re.search("(NAMUR_L =)\s+[0-9.-]+", text)
        N_L = int(p.group().split(' ')[-1])
        p = re.search("(NAMUR_S =)\s+[0-9.-]+", text)
        N_S = int(p.group().split(' ')[-1])



    #override the nao with the active space value
    nao_total = nao
    with open(glob("*.ei")[0],'r') as fid:
        line = filter(None, fid.readline().split('\t'))
        nao = int(line[1])/K_L

    print sdpfile, outfile,basfile
    factoredSol,dualsol = RDMUtil.sdpOut2(outfile)
    blockstruct = RDMUtil.getBlockStruct2(sdpfile)
    blockstruct = map(lambda x: 2*x, blockstruct)
    RDMs = RDMUtil.Factored2RDMs(blockstruct,factoredSol,'RR')
    D2Abasinfo,D2Sbasinfo,D1basinfo,G2basinfo = RDMUtil.getBas(basfile)

    #get complex RDM's
    cRDMs = []
    for i in range(len(blockstruct)):

        d = blockstruct[i]/2
        tmpr1 = RDMs[i][:d,:d]
        tmpr2 = RDMs[i][d:,d:]
        tmpi1 = RDMs[i][:d,d:]
        tmpi2 = -RDMs[i][d:,:d]
        cRDMs.append(tmpr1[:,:] + 1j*tmpi1[:,:])

    RDMs = cRDMs

    #########################################################################
    #
    #   reorder RDMs into canonical ordering
    #
    ########################################################################

    #Generate MO basis
    onebas = {}
    kvec = range(K_L)
    cnt = 0
    for k in range(K_L):
        for j in range(nao):
            onebas[cnt] = (kvec[k]%K_L, j)
            cnt += 1

    onebas_rev = dict(zip(onebas.values(), onebas.keys()))
    #get spin-adapted geminal basis sets
    d2a_bas = {}
    cnt = 0
    for i in range(nao*K_L):
        for j in range(i+1,nao*K_L):
            d2a_bas[cnt] = (i+1, j+1)
            cnt += 1

    d2s_bas = {}
    cnt = 0
    for i in range(nao*K_L):
        for j in range(i,nao*K_L):
            d2s_bas[cnt] = (i+1, j+1)
            cnt += 1

    d1_bas = dict(zip(range(nao*K_L), map(lambda x: x+1, range(nao*K_L) ) ) )

    d2a_bas_rev = dict(zip(d2a_bas.values(), d2a_bas.keys()))
    d2s_bas_rev = dict(zip(d2s_bas.values(), d2s_bas.keys()))
    d1_bas_rev = dict(zip(d1_bas.values(), d1_bas.keys()))


    #unwrap bas info from multiple D2A blocks to one D2A block from basfile 
    D2Abas = {}
    cnt = 0
    for kk in D2Abasinfo:
        for xx in range(len(kk)):
            D2Abas[cnt] = kk[xx]
            cnt += 1
    #unwrap bas info from multiple D2S blocks to one D2S block from basfile 
    D2Sbas = {}
    cnt = 0
    for kk in D2Sbasinfo:
        for xx in range(len(kk)):
            D2Sbas[cnt] = kk[xx]
            cnt += 1
    #unwrap bas info from multiple D1 blocks to one D1 block from basfile 
    D1bas = {}
    cnt = 0
    for kk in D1basinfo:
        for xx in range(len(kk)):
            D1bas[cnt] = kk[xx]
            cnt += 1

    #find out how to permute output D2A matrix to put into canonical ordering
    d2a_new_order = []
    for cidx, cgem in d2a_bas.iteritems():
        #for all cgem in correct order
        #find the index of the same geminal in the other basis ordering
        for didx, dgem in D2Abas.iteritems():
            if cgem == dgem:
                #row index of dgem 
                d2a_new_order.append(didx)

    #find out how to permute output D2S matrix to put into canonical ordering
    d2s_new_order = []
    for cidx, cgem in d2s_bas.iteritems():
        #for all cgem in correct order
        #find the index of the same geminal in the other basis ordering
        for didx, dgem in D2Sbas.iteritems():
            if cgem == dgem:
                #row index of dgem 
                d2s_new_order.append(didx)
    
    #find out how to permute output D1 matrix to put into canonical ordering
    d1_new_order = []
    for cidx, bas in d1_bas.iteritems():
        for didx, dbas in D1bas.iteritems():
            if bas == dbas:
                d1_new_order.append(didx)

    #just check that nothing crazy happened
    assert(len(d2s_new_order) == len(D2Sbas) )
    assert(len(d2a_new_order) ==  len(D2Abas) )
    assert(len(d1_new_order) ==  len(D1bas) )


    #block_diag to generate antisymmetric, symmetric, and D1
    D2A = RDMs[0]
    if len(D2Abasinfo) > 1:
        for i in range(1,len(D2Abasinfo)):
            D2A = block_diag(D2A, RDMs[i])
            
    D2S = RDMs[len(D2Abasinfo)]   
    if len(D2Sbasinfo) > 1:
        for i in range(1, len(D2Sbasinfo)):
            D2S = block_diag(D2S, RDMs[len(D2Abasinfo) + i])

    index = len(D2Abasinfo) + len(D2Sbasinfo)
    D1 = RDMs[index]
    if len(D1basinfo) > 1:
        for i in range(1, len(D1basinfo)):
            D1 = block_diag(D1, RDMs[index + i ])


    #now permute matrices to canonical ordering
    D2A = D2A[:,d2a_new_order]
    D2A = D2A[d2a_new_order,:]
    D2S = D2S[:,d2s_new_order]
    D2S = D2S[d2s_new_order,:]
    D1 = D1[:,d1_new_order]
    D1 = D1[d1_new_order,:]
    #REORDERED!

    D1Kblocks_tmp = []
    for i in range(K_L):
        D1Kblocks_tmp.append(D1[i*nao:(i+1)*nao,i*nao:(i+1)*nao])

    for i in range(K_L):
        w,v = np.linalg.eigh(D1Kblocks_tmp[i])
        print i, w[0], w[1]

    #########################################################################
    #
    #   Generate spin and even/odd symmetry adapting matrices
    #
    ########################################################################
    

    #spin adapting matrix
    U = np.zeros((nao*nao*K_L*K_L, nao*nao*K_L*K_L),dtype=complex)
    d2_bas = dict(zip(
                range(nao*nao*K_L*K_L), 
                list(product(
                    range(1,(nao*K_L)+1), 
                    range(1,(nao*K_L)+1)
                ) )
            ) )

    d2_bas_rev = dict(zip(d2_bas.values(), d2_bas.keys()))


    cnt = 0
    for xx in sorted(d2a_bas.keys()):
        i,j = d2a_bas[xx]
        x1 = d2_bas_rev[(i,j)]
        x2 = d2_bas_rev[(j,i)]
        U[x1, cnt ] = 1./np.sqrt(2)
        U[x2, cnt ] = -1./np.sqrt(2)
        cnt += 1

    for xx in sorted(d2s_bas.keys()):
        i,j = d2s_bas[xx]
        x1 = d2_bas_rev[(i,j)]
        x2 = d2_bas_rev[(j,i)]

        if x1==x2:
            U[x1 , cnt ] = 1.0
        else:
            U[x1, cnt] = 1./np.sqrt(2)
            U[x2, cnt] = 1./np.sqrt(2)

        cnt += 1

    #1 unpsin symmetry adapt
    D2SA = block_diag(D2A,D2S)
    D2ab = np.dot(U, np.dot(D2SA, U.conjugate().T))

    #extra transpose is because i wrote them in my c++ in rows...not cols
    coeff_mats = getCoeffs()
    Coeffs = block_diag(coeff_mats[0].T, coeff_mats[1].T)
    for i in range(2,len(coeff_mats)):
        Coeffs = block_diag(Coeffs, coeff_mats[i].T)
    Omats = getOverlaps()

   
    #generate K-overlaps
    KOverlaps = []
    for k in range(K_L):
        tmp_mat = np.zeros((nao_total,nao_total),dtype=complex)
        for m in range(-N_S, N_S+1):
            tmp_mat += Omats[m + N_S]*np.exp(0+1j*(m*k*2*np.pi/K_L))
        KOverlaps.append(tmp_mat)
    KS = block_diag(KOverlaps[0], KOverlaps[1])
    for i in range(2,len(KOverlaps)):
        KS = block_diag(KS, KOverlaps[i])

    tmp = np.dot(coeff_mats[0].conjugate(), np.dot(KOverlaps[0],
        coeff_mats[0].T))
    for x in range(tmp.shape[0]):
        for y in range(tmp.shape[1]):
            if np.linalg.norm(tmp[x,y]) < float(1.0E-14):
                tmp[x,y] = 0.0



    #Put correlated D1 back into uncorrelated D1
    #correlated matrices are 3x3 and correspond to the fifth - 7th indices
    D1K_blocks = []
    D1K_sigma1s_blocks = []
    D1K_sigma2s_blocks = []
    for i in range(K_L):
        tmp = np.zeros((nao_total, nao_total),dtype=complex)
        tmp[0,0] = 2.0
        tmp[1:3,1:3] = 2*D1[i*nao:(i+1)*nao,i*nao:(i+1)*nao]
        D1K_blocks.append(tmp)
        tmp2 = np.zeros((nao_total, nao_total),dtype=complex)
        tmp2[0,0] = 2.0
        D1K_sigma1s_blocks.append(tmp2)
        tmp3 = np.zeros((nao_total, nao_total),dtype=complex)
        tmp3[1:3,1:3] = 2*D1[i*nao:(i+1)*nao,i*nao:(i+1)*nao]
        D1K_sigma2s_blocks.append(tmp3)
    

    D1KMO = block_diag(D1K_blocks[0], D1K_blocks[1])
    D1KMO_s1 = block_diag(D1K_sigma1s_blocks[0], D1K_sigma1s_blocks[1])
    D1KMO_s2 = block_diag(D1K_sigma2s_blocks[0], D1K_sigma2s_blocks[1])
 
    for i in range(2,len(D1K_blocks)):
        D1KMO = block_diag(D1KMO, D1K_blocks[i])
        D1KMO_s1 = block_diag(D1KMO_s1, D1K_sigma1s_blocks[i])
        D1KMO_s2 = block_diag(D1KMO_s2, D1K_sigma2s_blocks[i])


    #rotate Dk(MO) to DK(AO)  Yay now we have that.  We can do the inverse
    #transform to generate our spatial density matrices
    D1AO = np.dot(Coeffs, np.dot(D1KMO,Coeffs.conjugate().T) )
    D1AO_s1 = np.dot(Coeffs, np.dot(D1KMO_s1,Coeffs.conjugate().T) )
    D1AO_s2 = np.dot(Coeffs, np.dot(D1KMO_s2,Coeffs.conjugate().T) )
    
    D1AO_blocks = []
    D1AO_s1_blocks = []
    D1AO_s2_blocks = []
    for i in range(K_L):
        D1AO_blocks.append(D1AO[i*nao_total:(i+1)*nao_total,i*nao_total:(i+1)*nao_total])
        D1AO_s1_blocks.append(D1AO_s1[i*nao_total:(i+1)*nao_total,i*nao_total:(i+1)*nao_total])
        D1AO_s2_blocks.append(D1AO_s2[i*nao_total:(i+1)*nao_total,i*nao_total:(i+1)*nao_total])
  

    XD1AO = []
    XD1AO_s1 = []
    XD1AO_s2 = []
    for m in range(-N_S, N_S+1):
        tmp = np.zeros((nao_total, nao_total),dtype=complex)
        tmp1 = np.zeros((nao_total, nao_total),dtype=complex)
        tmp2 = np.zeros((nao_total, nao_total),dtype=complex)
        for k in range(K_L):
            tmp += D1AO_blocks[k]*np.exp(0+1j*(m*2*np.pi*k/K_L))
            tmp1 += D1AO_s1_blocks[k]*np.exp(0+1j*(m*2*np.pi*k/K_L))
            tmp2 += D1AO_s2_blocks[k]*np.exp(0+1j*(m*2*np.pi*k/K_L))
        tmp *= (1./K_L)
        tmp1 *= (1./K_L)
        tmp2 *= (1./K_L)
        XD1AO.append(tmp.real)
        XD1AO_s1.append(tmp1.real)
        XD1AO_s2.append(tmp2.real)

    #check trace
    electrons = 0
    for n in range(2*N_S + 1):
        electrons += np.sum(XD1AO[n]*Omats[n].T)

    print "electron count ", electrons
    print "electrons in unit cell ", np.trace(np.dot(XD1AO[N_S], Omats[N_S]))

    pickle.dump(XD1AO, open("XD1AO_RDM.pkl","wb") )
    pickle.dump(XD1AO_s1, open("XD1AO_RDM_s1.pkl","wb") )
    pickle.dump(XD1AO_s2, open("XD1AO_RDM_s2.pkl","wb") )


    traced = 0
    Mulliken = []
    for i in range(2*N_S + 1):
        traced += np.trace(np.dot(XD1AO[i], Omats[i]))
        Mulliken.append(XD1AO[i]*Omats[i].T )
    #print traced
    GOP = np.zeros(basis_rank)
    for orb in range(basis_rank):
        for i in range(2*N_S + 1):
            GOP[orb] += np.sum(Mulliken[i][orb,:] )
    
    print GOP, np.sum(GOP)
         
