'''
Make the ei file from the Integrals.dat file and generate a makeSDP
'''

import sys
import os
from glob import glob
import re

def writePBSmakeSDP(runname, RDMlevel,nodes,ppn,cnt):


    #node_list = ['sequoia-0-0','sequoia-0-1', 'sequoia-0-2', 'sequoia-0-3']
    #print runname 
    runname += "makeSDP"
    Text = '###PBS submission Script\n\n'
    INPUT = '#PBS -N ' + runname + '\n'
    INPUT = INPUT + '#PBS -e ' + runname + '.e' + '\n'
    INPUT = INPUT + '#PBS -o ' + runname + '.o' + '\n'
    INPUT = INPUT + '#PBS -l nodes=%i:ppn=%i\n\n' % (nodes,ppn)
    #INPUT = INPUT + '#PBS -l nodes=%s:ppn=%i\n\n' % (node_list[cnt%4],ppn)



    runinfo = 'module load maple\n'    
    run = 'maple < makeSDP ' #> %s ' % (runname+'.log')
    

    setEnv = 'SCR_PATH=\"/state/partition1/scr/nick\"\n\n'
    setEnv = setEnv + 'if [ ! -e $SCR_PATH ]; then\nmkdir -p $SCR_PATH\nfi\n\n'
    setEnv = setEnv + 'cd $PBS_O_WORKDIR\n\n'

    
    Text = Text + INPUT + runinfo + setEnv + run

    fid = open(runname+'.pbs','w')
    fid.write(Text)
    fid.close() 



def writemakesdp(rdmlevel,orblist='NULL'):

    
    try:
        runname = glob('*.ei')[0].split('.')[0] 
    except IndexError:
        runname = "fail"
        print os.getcwd()
    print 'generating sdp inputs for run named: %s' % runname 
    
    text = '#input ei file;\n'
    input = 'str := \"%s\";\n' % runname
    input = input + 'ifile := \"%s/\"||str||\".ei\";\n' % (os.getcwd())
    output = '# output sdp file;\n'
    output = output + 'ofile := \"%s/\"||str||\"%s.sdp\";\n' %(os.getcwd(),rdmlevel)
    method = '# method (\"d\",\"dq\",\"dqg\", or \"dqgt\");\n'
    method = method + 'method := \"%s\";\n' % rdmlevel
    orb = '# list of orbital indices of h atoms ([null] or [1,2]);\n'
    orb = orb + 'nH := [%s];\n' % orblist
    bas = '# optional basis set file;\n'
    bas = bas + 'bfile := \"%s/\"||str||\"%s.bas\";\n' % (os.getcwd(),rdmlevel)
    text = text + input + output + method + orb + bas 

    text = text + '# sdprdm;\nkernelopts(printbytes=false);\n'
    text = text + 'read \"/share/apps/rdmchem/makesdp/r67H/BIN/sdpRDMv67H.m\";\n'
    text = text + 'makeSDP(ifile,ofile,method,nH,bfile);\n'

    fid = open('makeSDP','w')
    fid.write(text)
    fid.close() 


if __name__=="__main__":

    pwd = os.getcwd()

    if len(sys.argv) != 2:
        print "first input is directory"
        sys.exit()

    os.chdir(sys.argv[1])

    dirs = filter(os.path.isdir, os.listdir('./'))
    print dirs
    cnt = 0
    for dd in dirs:

        os.chdir(dd)
        #write makeSDP
        writemakesdp('DQG')
        writePBSmakeSDP(dd,'DQG',1,1,cnt)

        #open file and write eifile
        cnt += 1
        os.chdir("../")
        
